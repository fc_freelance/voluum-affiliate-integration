<?php

Route::group(['namespace' => 'VoluumAffiliate\Http\Controllers'], function()
{
    Route::get('/', function() { return redirect('admin'); });

    /** Admin Routes */
    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function()
    {
        Route::get('', function() { return redirect('admin/offers'); });

        Route::get('offers', 'OffersController@show');
        Route::post('offers', 'OffersController@add');
        Route::put('offers/:id', 'OffersController@update');
        Route::delete('offers/:id', 'OffersController@delete');
        Route::post('offers/add_all', 'OffersController@addNetworkOffers');
        Route::post('offers/update', 'OffersController@updateAllFromNetwork');

        Route::get('offer/new', 'OffersController@showSingleNew');
        Route::get('offer/internal', 'OffersController@showSingleInternal');
        Route::get('offer/external', 'OffersController@showSingleExternal');
        Route::get('offer/integrated', 'OffersController@showSingleIntegrated');

        Route::get('affiliate-networks', 'AffiliateNetworksController@show');
        Route::post('affiliate-networks', 'AffiliateNetworksController@add');
        Route::put('affiliate-networks', 'AffiliateNetworksController@update');

        Route::get('traffic-sources', 'TrafficSourcesController@show');
        Route::post('traffic-sources', 'TrafficSourcesController@add');
        Route::put('traffic-sources', 'TrafficSourcesController@update');

        Route::get('reports/live/campaigns', 'CampaignsController@showLiveReport');
    });

    /** API Routes */
    Route::group(['prefix' => 'api'], function()
    {
        Route::get('campaigns', 'CampaignsController@fromApiByPublisher');
    });
});
