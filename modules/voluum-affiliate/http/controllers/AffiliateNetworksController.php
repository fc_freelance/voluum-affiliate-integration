<?php

namespace VoluumAffiliate\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\networks\NetworksManager;
use VoluumAffiliate\Services\AffiliateNetworksService;
use VoluumAffiliate\Utils\Util;

/**
 * Class AffiliateNetworksController
 * @package VoluumAffiliate\Http\Controllers
 */
class AffiliateNetworksController extends Controller
{
    /** @var AffiliateNetworksService $networksService */
    protected $networksService;

    /**
     * @param AffiliateNetworksService $networksService
     */
    public function __construct(AffiliateNetworksService $networksService)
    {
        $this->networksService = $networksService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        // Get POST data from input
        $data = $request->input();

        // Transform the default null value for network code into real NULL
        if (isset($data['network_code']) && $data['network_code'] == NetworksManager::NO_API_NETWORK_CODE) {
            $data['network_code'] = null;
        }

        // Add it to DB
        $result = $this->networksService->add($data);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not save {$data['name']} affiliate network");

        // Show again the page
        return Redirect::action('\VoluumAffiliate\Http\Controllers\AffiliateNetworksController@show', $request->query());

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        // Get POST data from input
        $data = $request->input();

        // Transform the default null value for network code into real NULL
        if (isset($data['network_code']) && $data['network_code'] == NetworksManager::NO_API_NETWORK_CODE) {
            $data['network_code'] = null;
        }

        // Add it to DB
        $result = $this->networksService->update($data);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not update {$data['name']} affiliate network");

        // Show again the page
        return Redirect::action('\VoluumAffiliate\Http\Controllers\AffiliateNetworksController@show', $request->query());

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        // Get available data feed from Internal API
        $networks = $this->networksService->allFromApi();

        // Data from DB
        $dbNetworks = $this->networksService->dbPayloadToAffiliateNetworks($this->networksService->find());

        // Merge them
        $networks = $this->networksService->mergeCollections($networks, $dbNetworks);

        // Get implemented APIs
        $available_apis = $this->networksService->getImplemented();

        // Filter already used codes
        $dbNetworksCodes = array_map(function(AffiliateNetwork $entry) {
            return $entry->getNetworkCode();
        }, $dbNetworks);

        // Get only keys (codes), and not internal one
        $available_codes = array_filter(array_keys($available_apis), function($entry) use ($dbNetworksCodes) {
            return $entry != NetworksManager::INTERNAL_NETWORK && !in_array($entry, $dbNetworksCodes);
        });

        // Init view data
        $viewData = array(
            'networks' => $networks,
            'available_codes' => $available_codes
        );

        // Render view
        return view('voluum-affiliate::affiliate-networks', $viewData);
    }
}
