<?php

namespace VoluumAffiliate\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\networks\NetworksManager;
use VoluumAffiliate\Services\AffiliateNetworksService;
use VoluumAffiliate\Services\OffersService;
use VoluumAffiliate\utils\Util;
use VoluumAffiliate\utils\Countries;

/**
 * Class OffersController
 * @package VoluumAffiliate\Http\Controllers
 */
class OffersController extends Controller
{
    /** @const integer PAGINATION_ITEMS */
    const PAGINATION_ITEMS = 5;

    /** @var OffersService $service */
    protected $offersService;

    /** @var AffiliateNetworksService $networksService */
    protected $networksService;

    /** @var null|array $config */
    protected $config = null;

    /**
     * @param AffiliateNetworksService $networksService
     * @param OffersService $offersService
     */
    public function __construct(AffiliateNetworksService $networksService, OffersService $offersService)
    {
        $this->offersService = $offersService;
        $this->networksService = $networksService;

        /** @var array $config */
        $config = config('voluum-affiliate');

        // Get full config as object!!
        $this->config = json_decode(json_encode($config));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        // Get data from input
        $data = $request->input();

        // Get payload from form data as offer
        /** @var Offer $offer */
        $offer = $this->offersService->offerFormToOffer($data);

        // Include affiliate network id from qs into data if not set
        if (!isset($data['affiliate_network_id']) && ($afnid = $request->query('affiliate_network_id', null))) {
            $offer->setAffiliateNetworkId($afnid);
        }

        // Check files
        if ($imgFile = $request->file('preview_image_file')) {

            // Move to local path
            $imgFile->move($this->config->assets->local_path, $imgFile->getClientOriginalName());

            // And set into offer
            $offer->setPreviewImage($this->config->assets->base_url.'/'.$imgFile->getClientOriginalName());
        }

        // Check files
        if ($creativesFile = $request->file('creatives_file')) {

            // Move to local path
            $creativesFile->move($this->config->assets->local_path, $creativesFile->getClientOriginalName());

            // And set into offer
            $offer->setCreativesUrl($this->config->assets->base_url.'/'.$creativesFile->getClientOriginalName());
        }

        // Call service and get created offer, if any
        $result = $this->offersService->addOffer($offer);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not add offer {$offer->getName()}");

        // Redirect to show
        return Redirect::to($request->get('redirect_url', '/'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        // Get data from input
        $data = $request->input();

        // Get payload from form data as offer
        /** @var Offer $offer */
        $offer = $this->offersService->offerFormToOffer($data);

        // Include affiliate network id from qs into data if not set
        if (!isset($data['affiliate_network_id']) && ($afnid = $request->query('affiliate_network_id', null))) {
            $offer->setAffiliateNetworkId($afnid);
        }

        // Check files
        if ($imgFile = $request->file('preview_image_file')) {

            // Move to local path
            $imgFile->move($this->config->assets->local_path, $imgFile->getClientOriginalName());

            // And set into offer
            $offer->setPreviewImage($this->config->assets->base_url.'/'.$imgFile->getClientOriginalName());
        }

        // Check files
        if ($creativesFile = $request->file('creatives_file')) {

            // Move to local path
            $creativesFile->move($this->config->assets->local_path, $creativesFile->getClientOriginalName());

            // And set into offer
            $offer->setCreativesUrl($this->config->assets->base_url.'/'.$creativesFile->getClientOriginalName());
        }

        // Call service and get created offer, if any
        $result = $this->offersService->updateOffer($offer);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not update offer {$offer->getName()}");

        // Redirect to show
        return Redirect::to($request->get('redirect_url', '/'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        // Get data from request
        $data = $request->all();

        // If not set affiliate network, set it
        if (!isset($data['affiliate_network_id'])) {
            $offer = $this->offersService->dbPayloadToOffer($this->offersService->getIntegratedOffer($data['id']));
            $data['affiliate_network_id'] = $offer->getAffiliateNetworkId();
        }

        // Call service for offer deletion
        $result = $this->offersService->deleteOffer($data);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not delete offer {$data['name']}");

        // Redirect to show
        return Redirect::to($request->get('redirect_url', '/'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addNetworkOffers(Request $request)
    {
        // Check if qs with proper data
        if ($request->has('affiliate_network_id')) {

            // Add offers from network
            $offers = $this->offersService->addNetworkOffers($request->query());

            // Set relevant flash messages
            Util::setServiceFeedbackFlashMessages($request, $offers, "Could not add offers from {$request->query('affiliate_network_id')} network");

        // No affiliate network id, error
        } else {
            $request->session()->flash('message', array('severity' => 'danger', 'body' => 'Missing affiliate network for adding offers'));
        }

        return Redirect::action('\VoluumAffiliate\Http\Controllers\OffersController@show', $request->query());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        // Get query string filters
        $filters = $request->query();

        // Init view data
        $viewData = array();

        // Get all integrated networks
        $networks = $this->networksService->allIntegrated();

        // Check if got networks
        if (!empty($networks)) {

            // Set first one as default selected
            /** @var AffiliateNetwork $selectedNetwork */
            $selectedNetwork = $networks[0];

            // Check if filter provided in qs
            if (isset($filters['affiliate_network_id'])) {

                // Filter networks by id
                $filteredNetworks = array_values(array_filter($networks, function (AffiliateNetwork $entry) use ($filters) {
                    return $entry->getId() == $filters['affiliate_network_id'];
                }));

                // If got filtered networks, set the (hopefully unique) only one
                if (!empty($filteredNetworks)) {
                    $selectedNetwork = $filteredNetworks[0];

                } else {
                    $request->session()->flash('message', array('severity' => 'danger', 'body' => 'Network not supported'));
                }

            // No filter provided, but we need to manually add it with default selected network
            } else {

                // Render view with just networks
                return view('voluum-affiliate::offers', array('networks' => $networks));
            }

            // Let's force pagination .... mmmm
            $filtersWithPagination = array_merge(array(
                'page' =>  1,
                'items_per_page' => NetworksManager::ITEMS_PER_PAGE
            ), $filters);

            // Get affiliate offers for selected network if there's an API
            if ($selectedNetwork->hasAPI()) {
                $networkOffers = $this->offersService->getNetworkOffers($selectedNetwork->getNetworkCode(), $filtersWithPagination);

            // No API, get internal offers and filter by selected network
            } else {
                $networkOffers = $this->offersService->getInternalOffers();
                $networkOffers = array_filter($networkOffers['offers'], function(Offer $entry) use ($selectedNetwork) {
                    return $entry->getInternalNetworkId() == $selectedNetwork->getInternalId();
                });
                $networkOffers = array('offers' => $networkOffers);
            }

            // Filter if search provided
            if (isset($filters['search']) && !empty($search = $filters['search'])) {
                $networkOffers['offers'] = array_values(array_filter($networkOffers['offers'], function (Offer $entry) use ($search) {
                    return str_contains(strtolower($entry->getName() ?: $entry->getSourceName()), strtolower($search));
                }));
            }

            // Unset pagination from filters, they go to DB query
            unset($filters['page']);
            unset($filters['items_per_page']);
            unset($filters['_token']);
            unset($filters['search']);

            // Get integrated offers (in DB) using the filters
            $integratedOffers = $this->offersService->getIntegratedOffers($filters);

            // Init offers collection
            $offers = array();

            // Loop over affiliate/internal offers
            /** @var Offer $ntOffer */
            foreach($networkOffers['offers'] as $ntOffer) {

                // Set this valid one to non integrated one by default
                $currOffer = $ntOffer;

                // Filter integrated offers (in DB) which match the affiliate/internal one (hopefully only one)
                $filteredOffers = array_values(array_filter($integratedOffers, function($entry) use ($ntOffer, $request) {
                    return $ntOffer->isExternal() && $ntOffer->getExternalId() == $entry->external_id ||
                           $ntOffer->isInternal() && $ntOffer->getInternalId() == $entry->internal_id;
                }));

                // Offer matched, so we already got it integrated
                if (!empty($filteredOffers)) {

                    /** @var Offer $dbOffer */
                    $dbOffer = $this->offersService->dbPayloadToOffer($filteredOffers[0]);

                    // Only if coming from affiliate API
                    if ($currOffer->isExternal()) {

                        // Get it from Internal API
                        /** @var Offer $vOffer */
                        $vOffer = $this->offersService->getInternalOffer($dbOffer->getInternalId());

                        // And merge it
                        $currOffer = $this->offersService->mergeInternalOfferWithOffer($vOffer, $currOffer);
                    }

                    // Merge with DB one
                    $currOffer = $this->offersService->mergeDbOfferWithOffer($dbOffer, $currOffer);
                }

                // Let's set the affiliate network id anyway
                $currOffer->setAffiliateNetworkId($selectedNetwork->getId());

                // Add all countries if not set
                if (empty($currOffer->getCountries())) {
                    $currOffer->setCountries(Countries::allCodes());
                }

                // Autoselect if only one country available and non integrated && API offer
                if (!$currOffer->isIntegrated() && $currOffer->isExternal() && count($countries = $currOffer->getCountries()) == 1) {
                    $currOffer->setCountry($countries[0]);
                }

                // Add valid offer to collection
                $offers[] = $currOffer;
            }

            // Set view data
            $viewData = array(
                'networks' => $networks,
                'selected_network' => $selectedNetwork,
                'offers' => $offers
            );

            // Add pagination if exists ... buff .. with calculations
            if (isset($networkOffers['pagination'])) {
                $viewData['pagination'] = $networkOffers['pagination'];
                $totalItems = $viewData['pagination']['count'];
                $currPage = $viewData['pagination']['page'];
                $totalPages = ($totalItems < $filtersWithPagination['items_per_page']) ? $currPage : $viewData['pagination']['pages_count'];
                $viewData['pagination']['pages_count'] = $totalPages;
                $count = min(static::PAGINATION_ITEMS, $totalPages) - 1;
                $min = $max = $currPage;
                while ($count) {
                    if ($min - 1 > 0) {
                        $min--;
                        $count--;
                    }
                    if ($max + 1 <= $totalPages) {
                        $max++;
                        $count--;
                    }
                }
                $viewData['pagination']['page_min'] = $min;
                $viewData['pagination']['page_max'] = $max;
            }
        }

        // Render view with data
        return view('voluum-affiliate::offers', $viewData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSingleNew(Request $request)
    {
        // Call with new offer
        return $this->showSingle($request, new Offer());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSingleInternal(Request $request)
    {
        // Get internal id from qs
        $internal_id = $request->get('internal_id', null);

        // If empty, just redirect
        if (empty($internal_id)) return Redirect::to($request->get('redirect_url', '/'));

        // Get offer from internal API
        /** @var Offer $offer */
        $offer = $this->offersService->getInternalOffer($internal_id);

        // Get affiliate network id from qs if any, then set to offer
        if ($network_id = $request->get('network_id', null)) {
            $offer->setAffiliateNetworkId($network_id);
        }

        // Call with offer
        return $this->showSingle($request, $offer);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSingleExternal(Request $request)
    {
        // Get external id from qs
        $external_id = $request->get('external_id', null);

        // If empty, just redirect
        if (empty($external_id)) return Redirect::to($request->get('redirect_url', '/'));

        // Get affiliate network id from qs
        $network_id = $request->get('network_id', null);

        // If empty, just redirect
        if (empty($network_id)) return Redirect::to($request->get('redirect_url', '/'));

        // Get pagination ....
        $page = request()->query('page', 1);
        $items_per_page = request()->query('items_per_page', NetworksManager::ITEMS_PER_PAGE);

        // Get offer from affiliate network
        $offer = $this->offersService->getAffiliateNetworkOffer($network_id, $external_id, array('page' => $page, 'items_per_page' => $items_per_page));

        // Call with offer
        return $this->showSingle($request, $offer);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSingleIntegrated(Request $request)
    {
        // Get external id from qs
        $id = $request->get('id', null);

        // If empty, just redirect
        if (empty($id)) return Redirect::to($request->get('redirect_url', '/'));

        // Get offer from DB
        /** @var Offer $offer */
        $offer = $this->offersService->dbPayloadToOffer($this->offersService->getIntegratedOffer($id));

        // Check if external
        if ($offer->isExternal()) {

            // Get pagination ....
            $page = request()->query('page', 1);
            $items_per_page = request()->query('items_per_page', NetworksManager::ITEMS_PER_PAGE);

             // Get offer from there
            /** @var Offer $offer */
            $afOffer = $this->offersService->getAffiliateNetworkOffer($offer->getAffiliateNetworkId(), $offer->getExternalId(), array('page' => $page, 'items_per_page' => $items_per_page));

            // Merge data into
            $offer = $this->offersService->mergeExternalOfferWithOffer($afOffer, $offer);
        }

        // Check if internal
        if ($offer->isInternal()) {

            // Get offer from there
            /** @var Offer $iOffer */
            $iOffer = $this->offersService->getInternalOffer($offer->getInternalId());

            // Merge data into
            $offer = $this->offersService->mergeInternalOfferWithOffer($iOffer, $offer);
        }

        // Call with offer
        return $this->showSingle($request, $offer);
    }

    /**
     * @param Request $request
     * @param Offer $offer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function showSingle(Request $request, Offer $offer)
    {
        // Get all integrated networks
        $networks = $this->networksService->allIntegrated();

        // Init view data
        $viewData = array(
            'redirect_url' => $request->get('redirect_url', '/'),
            'offer' => $offer,
            'networks' => $networks,
            'selected_network' => null
        );

        // Check if got networks
        if (!empty($networks)) {

            // Check if filter provided in qs
            if ($network_id = $offer->getAffiliateNetworkId()) {

                // Filter networks by id
                $filteredNetworks = array_values(array_filter($networks, function (AffiliateNetwork $entry) use ($network_id) {
                    return $entry->getId() === $network_id;
                }));

                // If got filtered networks, set the (hopefully unique) only one
                if (!empty($filteredNetworks)) {
                    $viewData['selected_network'] = $filteredNetworks[0];
                }
            }
        }

        // Render view with data
        return view('voluum-affiliate::offer', $viewData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateAllFromNetwork(Request $request)
    {
        // Check if not network
        if (!$request->has('affiliate_network_internal_id')) {

            // Set error message
            $request->session()->flash('message', array('severity' => 'warning', 'body' => 'No network for updating'));

        // Got network
        } else {

            // Get network from input
            $network = $request->input('affiliate_network_internal_id');

            // Call service
            $result = $this->offersService->updateNetworkOffers($network);

            // Set message if non empty result
            if (!empty($result)) {
                $request->session()->flash('message', array('severity' => 'info', 'body' => $result));
            }
        }

        // Call other controller
        // TODO: Change this to its own results view?
        return Redirect::action('\VoluumAffiliate\Http\Controllers\OffersController@show', $request->query());
    }
}
