<?php

namespace VoluumAffiliate\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use VoluumAffiliate\Models\TrafficSource;
use VoluumAffiliate\Services\CampaignsService;
use VoluumAffiliate\Services\OffersService;
use VoluumAffiliate\Services\TrafficSourcesService;

/**
 * Class CampaignsController
 * @package VoluumAffiliate\Http\Controllers
 */
class CampaignsController extends Controller
{
    /** @var CampaignsService $campaignsService */
    protected $campaignsService;

    /** @var TrafficSourcesService $sourcesService */
    protected $sourcesService;

    /** @var OffersService $offersService */
    protected $offersService;

    /**
     * @param CampaignsService $campaignsService
     * @param TrafficSourcesService $sourcesService
     * @param OffersService $offersService
     */
    public function __construct(CampaignsService $campaignsService, TrafficSourcesService $sourcesService, OffersService $offersService)
    {
        $this->campaignsService = $campaignsService;
        $this->sourcesService = $sourcesService;
        $this->offersService = $offersService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fromApiByPublisher(Request $request)
    {
        // Check if publisher id
        if (!$request->has('publisher_id')) {
            return response()->json([
                'status' => 400,
                'error' => 'Missing publisher id'
            ], 400);
        }

        // Get qs
        $qs = $request->query();

        // Get traffic source from API
        /** @var TrafficSource $tSource */
        $tSource = $this->sourcesService->getFromApi($qs['publisher_id']);

        // Check if empty, and return error if so
        if (empty($tSource)) {
            return response()->json([
                'status' => 400,
                'error' => 'Invalid publisher id'
            ], 400);
        }

        // FIXME: Remove this if adopting the regular API rather than report one is OK (so currently we don't use this)
//        // TODO: Achtung!!! We're matching against name!! The reason: Voluum reporting API does not offer this id and name is unique ....
//        $qs['traffic_source_name'] = $tSource->getName();
//
//        // Get all campaigns from API
//        // TODO: Get all? Or maybe only active ones? .... right now getting all with 'active' boolean field
//        $result = $this->campaignsService->reportToPayload($this->campaignsService->allFromReportingApi($qs));

        // Get all from regular API, we need the paths with offers
        // TODO: Achtung!!! This fetches only ACTIVE campaigns!!!
        $campaigns = $this->campaignsService->allFromApi();

        // Now filter them for an API response
        $campaigns = $this->campaignsService->filterPublisherCampaigns($campaigns, $tSource, $qs);

        // Get offers from DB
        // TODO: Get only for ids in campaigns above, which one is faster??
        $offers = $this->offersService->dbPayloadToOffers($this->offersService->getIntegratedOffers());

        // Now create final payload
        $campaigns = $this->campaignsService->generatePublisherCampaignsPayload($campaigns, $offers);

        // Return response data
        return response()->json([
            'status' => 200,
            'count' => count($campaigns),
            'campaigns' => $campaigns
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLiveReport(Request $request)
    {
        // Initialize view data
        $viewData = array('campaigns' => array(), 'report' => array());

        // Get available campaigns
        $campaigns = $this->campaignsService->allFromReportingApi();

        // Check if no campaigns
        if (!empty($campaigns->rows)) {

            // Assings campaigns to select in view
            $viewData['campaigns'] = $campaigns->rows;

            // Get qs params
            $params = $request->query();

            // If no campaign id in qs, set it to first one
            if (!isset($params['campaign_id'])) {
                $params['campaign_id'] = $campaigns->rows[0]->campaignId;
            }

            // Set selected campaign id to that of qs
            $viewData['selected_campaign_id'] = $params['campaign_id'];

            // Get live report for campaign
            $result = $this->campaignsService->liveReportByCampaign($params);

            // Set report into view
            $viewData['report'] = $result->rows;
        }

        // Return view with data
        return view('voluum-affiliate::live-report-campaigns', $viewData);
    }
}
