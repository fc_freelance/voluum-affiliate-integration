<?php

namespace VoluumAffiliate\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use VoluumAffiliate\Models\TrafficSource;
use VoluumAffiliate\Services\TrafficSourcesService;
use VoluumAffiliate\Utils\Util;

/**
 * Class TrafficSourcesController
 * @package VoluumAffiliate\Http\Controllers
 */
class TrafficSourcesController extends Controller
{
    /** @var TrafficSourcesService $sourcesService */
    protected $sourcesService;

    /**
     * @param TrafficSourcesService $sourcesService
     */
    public function __construct(TrafficSourcesService $sourcesService)
    {
        $this->sourcesService = $sourcesService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        // Get POST data from input
        $data = $request->input();

        // Add it to DB
        $result = $this->sourcesService->add($data);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not save {$data['name']} traffic source");

        // Show again the page
        return Redirect::action('\VoluumAffiliate\Http\Controllers\TrafficSourcesController@show', $request->query());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        // Get POST data from input
        $data = $request->input();

        // Add it to DB
        $result = $this->sourcesService->update($data);

        // Set relevant flash messages
        Util::setServiceFeedbackFlashMessages($request, $result, "Could not update {$data['name']} traffic source");

        // Show again the page
        return Redirect::action('\VoluumAffiliate\Http\Controllers\TrafficSourcesController@show', $request->query());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        // Get available affiliate networks
        $sources = $this->sourcesService->findFromApi();

        // Data from DB
        $dbSources = $this->sourcesService->dbPayloadToTrafficSources($this->sourcesService->find());

        // Loop over all API data
        /** @var TrafficSource $entry */
        foreach($sources as $entry) {

            // Try to filter DB ones
            $filtered = array_values(array_filter($dbSources, function(TrafficSource $item) use ($entry) {
                return $entry->getInternalId() == $item->getInternalId();
            }));

            // Check if got something
            if (!empty($filtered)) {

                /** @var TrafficSource $fEntry */
                $fEntry = $filtered[0];

                // Merge with DB one
                $entry = $this->sourcesService->mergeDbSourceWithSource($fEntry, $entry);
            }
        }

        // Init view data
        $viewData = array(
            'sources' => $sources
        );

        // Render View
        return view('voluum-affiliate::traffic-sources', $viewData);
    }
}
