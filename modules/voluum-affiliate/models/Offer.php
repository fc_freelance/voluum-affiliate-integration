<?php

namespace VoluumAffiliate\Models;
use stdClass;
use VoluumAffiliate\utils\Countries;
use VoluumAffiliate\Utils\Util;

/**
 * Class Offer
 * @package VoluumAffiliate\Models
 */
class Offer
{
    /** @var string|integer $id */
    protected $id;

    /** @var string|integer $internal_id */
    protected $internal_id;

    /** @var string|integer $external_id */
    protected $external_id;

    /** @var string|integer $internal_network_id */
    protected $internal_network_id;

    /** @var string|integer $affiliate_network_id */
    protected $affiliate_network_id;

    /** @var string $name */
    protected $name;

    /** @var string url */
    protected $url;

    /** @var array|null $countries */
    protected $countries;

    /** @var string|null $country */
    protected $country;

    /** @var number $payout */
    protected $payout;

    /** @var string $source_name */
    protected $source_name;

    /** @var string $preview_url */
    protected $preview_url;

    /** @var string $preview_image */
    protected $preview_image;

    /** @var array $platforms */
    protected $platforms;

    /** @var integer $caps */
    protected $caps;

    /** @var string $cost_model */
    protected $cost_model;

    /** @var boolean $incent */
    protected $incent;

    /** @var boolean $operator_based */
    protected $operator_based;

    /** @var string $creatives_url */
    protected $creatives_url;

    /** @var boolean $auto */
    protected $auto;

    /** @var boolean $auto_payout */
    protected $auto_payout;

    /**
     * @param array|stdClass $data
     */
    public function __construct($data = array())
    {
        // Loop over data and parse to properties
        foreach($data as $key => $value) {
            if (property_exists('VoluumAffiliate\Models\Offer', $key)) {
                $this->{$key} = $value;
            }
        }

        // Add all countries if not set
        if (empty($this->countries)) {
            $this->countries = Countries::allCodes();
        }
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|string
     */
    public function getExternalId()
    {
        return $this->external_id;
    }

    /**
     * @param int|string $external_id
     */
    public function setExternalId($external_id)
    {
        $this->external_id = $external_id;
    }

    /**
     * @return int|string
     */
    public function getInternalId()
    {
        return $this->internal_id;
    }

    /**
     * @param int|string $internal_id
     */
    public function setInternalId($internal_id)
    {
        $this->internal_id = $internal_id;
    }

    /**
     * @return int|string
     */
    public function getInternalNetworkId()
    {
        return $this->internal_network_id;
    }

    /**
     * @param int|string $internal_network_id
     */
    public function setInternalNetworkId($internal_network_id)
    {
        $this->internal_network_id = $internal_network_id;
    }

    /**
     * @return int|string
     */
    public function getAffiliateNetworkId()
    {
        return $this->affiliate_network_id;
    }

    /**
     * @param int|string $affiliate_network_id
     */
    public function setAffiliateNetworkId($affiliate_network_id)
    {
        $this->affiliate_network_id = $affiliate_network_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array|null
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param array|null $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return null|string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param null|string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return number
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * @param number $payout
     */
    public function setPayout($payout)
    {
        $this->payout = $payout;
    }

    /**
     * @return string
     */
    public function getSourceName()
    {
        return $this->source_name;
    }

    /**
     * @param string $source_name
     */
    public function setSourceName($source_name)
    {
        $this->source_name = $source_name;
    }

    /**
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->preview_url;
    }

    /**
     * @param string $preview_url
     */
    public function setPreviewUrl($preview_url)
    {
        $this->preview_url = $preview_url;
    }

    /**
     * @return string
     */
    public function getPreviewImage()
    {
        return $this->preview_image;
    }

    /**
     * @param string $preview_image
     */
    public function setPreviewImage($preview_image)
    {
        $this->preview_image = $preview_image;
    }

    /**
     * @return array
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }

    /**
     * @param array $platforms
     */
    public function setPlatforms($platforms)
    {
        $this->platforms = $platforms;
    }

    /**
     * @return int
     */
    public function getCaps()
    {
        return $this->caps;
    }

    /**
     * @param int $caps
     */
    public function setCaps($caps)
    {
        $this->caps = $caps;
    }

    /**
     * @return string
     */
    public function getCostModel()
    {
        return $this->cost_model;
    }

    /**
     * @param string $cost_model
     */
    public function setCostModel($cost_model)
    {
        $this->cost_model = $cost_model;
    }

    /**
     * @return boolean
     */
    public function isIncent()
    {
        return $this->incent;
    }

    /**
     * @param boolean $incent
     */
    public function setIncent($incent)
    {
        $this->incent = $incent;
    }

    /**
     * @return boolean
     */
    public function isOperatorBased()
    {
        return $this->operator_based;
    }

    /**
     * @param boolean $operator_based
     */
    public function setOperatorBased($operator_based)
    {
        $this->operator_based = $operator_based;
    }

    /**
     * @return string
     */
    public function getCreativesUrl()
    {
        return $this->creatives_url;
    }

    /**
     * @param string $creatives_url
     */
    public function setCreativesUrl($creatives_url)
    {
        $this->creatives_url = $creatives_url;
    }

    /**
     * @return bool
     */
    public function isIntegrated()
    {
        return !empty($this->id);
    }

    /**
     * @return bool
     */
    public function isInternal()
    {
        return !empty($this->internal_id);
    }

    /**
     * @return boolean
     */
    public function isAuto()
    {
        return $this->auto;
    }

    /**
     * @param boolean $auto
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;
    }

    /**
     * @return boolean
     */
    public function isAutoPayout()
    {
        return $this->auto_payout;
    }

    /**
     * @param boolean $auto_payout
     */
    public function setAutoPayout($auto_payout)
    {
        $this->auto_payout = $auto_payout;
    }

    /**
     * @return bool
     */
    public function isExternal()
    {
        return !empty($this->external_id);
    }

    /**
     * @return int|string
     */
    public function getComposedName()
    {
        // If got name, return it
        if ($this->name) return $this->name;

        // Form name and return it otherwise
        $name = $this->source_name ?: $this->external_id;
        $name .= ($this->platforms && count($this->platforms)) ? ('_'.join(',',$this->platforms)) : '';
        $name .= $this->cost_model ? ('_'.$this->cost_model) : '';
        $name .= $this->incent ? '_Incent' : '';
        return $name;
    }
}