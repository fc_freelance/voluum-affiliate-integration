<?php

namespace VoluumAffiliate\Models;
use stdClass;
use VoluumAffiliate\networks\NetworksManager;

/**
 * Class AffiliateNetwork
 * @package VoluumAffiliate\Models
 */
class AffiliateNetwork
{
    /** @var string|integer $id */
    protected $id;

    /** @var string|integer $internal_id */
    protected $internal_id;

    /** @var string $network_code */
    protected $network_code;

    /** @var string $name */
    protected $name;

    /** @var boolean $operator_based */
    protected $operator_based;

    /** @var boolean $auto */
    protected $auto;

    /** @var boolean $auto_payout */
    protected $auto_payout;

    /**
     * @param array|stdClass $data
     */
    public function __construct($data = array()) {
        foreach($data as $key => $value) {
            if (property_exists('VoluumAffiliate\Models\AffiliateNetwork', $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|string
     */
    public function getInternalId()
    {
        return $this->internal_id;
    }

    /**
     * @param int|string $internal_id
     */
    public function setInternalId($internal_id)
    {
        $this->internal_id = $internal_id;
    }

    /**
     * @return string
     */
    public function getNetworkCode()
    {
        return $this->network_code ?: null;
    }

    /**
     * @param string $network_code
     */
    public function setNetworkCode($network_code)
    {
        $this->network_code = $network_code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isOperatorBased()
    {
        return $this->operator_based;
    }

    /**
     * @param boolean $operator_based
     */
    public function setOperatorBased($operator_based)
    {
        $this->operator_based = $operator_based;
    }

    /**
     * @return boolean
     */
    public function isAuto()
    {
        return $this->auto;
    }

    /**
     * @param boolean $auto
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;
    }

    /**
     * @return boolean
     */
    public function isAutoPayout()
    {
        return $this->auto_payout;
    }

    /**
     * @param boolean $auto_payout
     */
    public function setAutoPayout($auto_payout)
    {
        $this->auto_payout = $auto_payout;
    }

    /**
     * @return bool
     */
    public function hasAPI()
    {
        return !empty($this->network_code) && $this->network_code !== NetworksManager::NO_API_NETWORK_CODE;
    }
}