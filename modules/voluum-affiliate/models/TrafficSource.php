<?php

namespace VoluumAffiliate\Models;
use stdClass;

/**
 * Class TrafficSource
 * @package VoluumAffiliate\Models
 */
class TrafficSource
{
    /** @var string|integer $id */
    protected $id;

    /** @var string|integer $internal_id */
    protected $internal_id;

    /** @var string $name */
    protected $name;

    /** @var string $postback_url */
    protected $postback_url;

    /** @var string $api_endpoint */
    protected $api_endpoint;

    /** @var array $platforms */
    protected $platforms;

    /** @var array $countries */
    protected $countries;

    /** @var string $cost_model */
    protected $cost_model;

    /** @var integer $min_caps */
    protected $min_caps;

    /** @var boolean $incent */
    protected $incent;

    /** @var boolean $auto */
    protected $auto;

    /** @var boolean $auto_payout */
    protected $auto_payout;

    /**
     * @param array|stdClass $data
     */
    public function __construct($data = array()) {
        foreach($data as $key => $value) {
            if (property_exists('VoluumAffiliate\Models\TrafficSource', $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|string
     */
    public function getInternalId()
    {
        return $this->internal_id;
    }

    /**
     * @param int|string $internal_id
     */
    public function setInternalId($internal_id)
    {
        $this->internal_id = $internal_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPostbackUrl()
    {
        return $this->postback_url;
    }

    /**
     * @param string $postback_url
     */
    public function setPostbackUrl($postback_url)
    {
        $this->postback_url = $postback_url;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->api_endpoint;
    }

    /**
     * @param string $api_endpoint
     */
    public function setApiEndpoint($api_endpoint)
    {
        $this->api_endpoint = $api_endpoint;
    }

    /**
     * @return array
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }

    /**
     * @param array $platforms
     */
    public function setPlatforms($platforms)
    {
        $this->platforms = $platforms;
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param array $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return string
     */
    public function getCostModel()
    {
        return $this->cost_model;
    }

    /**
     * @param string $cost_model
     */
    public function setCostModel($cost_model)
    {
        $this->cost_model = $cost_model;
    }

    /**
     * @return int
     */
    public function getMinCaps()
    {
        return $this->min_caps;
    }

    /**
     * @param int $min_caps
     */
    public function setMinCaps($min_caps)
    {
        $this->min_caps = $min_caps;
    }

    /**
     * @return boolean
     */
    public function isIncent()
    {
        return $this->incent;
    }

    /**
     * @param boolean $incent
     */
    public function setIncent($incent)
    {
        $this->incent = $incent;
    }

    /**
     * @return boolean
     */
    public function isAuto()
    {
        return $this->auto;
    }

    /**
     * @param boolean $auto
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;
    }

    /**
     * @return boolean
     */
    public function isAutoPayout()
    {
        return $this->auto_payout;
    }

    /**
     * @param boolean $auto_payout
     */
    public function setAutoPayout($auto_payout)
    {
        $this->auto_payout = $auto_payout;
    }
}