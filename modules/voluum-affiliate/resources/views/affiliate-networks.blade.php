@extends('voluum-affiliate::main')

@section('title', 'Affiliate Networks')

@section('content')

    <section class="va-affiliate-networks table-responsive">

        @if (isset($networks) && count($networks))
            <table class="table table-condensed table-striped va-table va-affiliate-networks-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>API</th>
                    <th class="va-affiliate-network-cell-operator_based">Operator Restricted</th>
                    <th class="va-affiliate-network-cell-auto">Auto</th>
                    <th class="va-affiliate-network-cell-auto_payout">Auto Payout</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($networks as $network)
                    <tr>
                        <form method='post'
                              action="{{ action('\VoluumAffiliate\Http\Controllers\AffiliateNetworksController@'.(empty($network->getId()) ? 'add' : 'update'), [
                            'id' => $network->getId()
                        ]) }}">

                            <td class="va-affiliate-network-cell-name">
                                <input type="text" name="name" placeholder="Name" value="{{ $network->getName() }}" required class="form-control" disabled>
                            </td>

                            <td class="va-affiliate-network-cell-code">
                                <select name="network_code" id="network_code" class="form-control" required @if (!empty($network->getId())) disabled @endif>
                                    <option value=""></option>
                                    <option value="{{ \VoluumAffiliate\networks\NetworksManager::NO_API_NETWORK_CODE }}">None</option>
                                    @if (empty($network->getId()))
                                        @foreach ($available_codes as $code)
                                            <option value="{{ $code }}" @if($network->getNetworkCode() == $code) selected @endif>
                                                {{ $code }}
                                            </option>
                                        @endforeach
                                    @else
                                        <option value="{{ empty($network->getNetworkCode()) ? \VoluumAffiliate\networks\NetworksManager::NO_API_NETWORK_CODE : $network->getNetworkCode() }}" selected>
                                            {{ empty($network->getNetworkCode()) ? 'None' : $network->getNetworkCode() }}
                                        </option>
                                    @endif
                                </select>
                            </td>

                            <td class="va-affiliate-network-cell-operator_based">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="operator_based" value="1" @if($network->isOperatorBased()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-affiliate-network-cell-auto">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="auto" value="1" @if($network->isAuto()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-affiliate-network-cell-auto_payout">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="auto_payout" value="1" @if($network->isAutoPayout()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-affiliate-network-cell-add">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="internal_id" value="{{ $network->getInternalId() }}">
                                <input type="hidden" name="name" value="{{ $network->getName() }}">
                                @if(!empty($network->getId()))
                                    <input type="hidden" name="network_code" value="{{ $network->getNetworkCode() }}">
                                    <input type="hidden" name="_method" value="put">
                                @endif
                                <input type="submit" class="btn btn-{{ empty($network->getId()) ? 'primary' : 'warning' }}"
                                       value="{{ empty($network->getId()) ? 'Add' : 'Update' }}">
                            </td>

                        </form>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="center-block text-center">
                No Affiliate Networks found!!!
            </div>
        @endif

    </section>

@endsection