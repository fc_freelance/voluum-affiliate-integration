@if(Session::has('message'))
    <section class="container-fluid va-flash-messages">
        <div class="va-flash-message va-severity-{{ session('message')['severity'] }} alert alert-{{ session('message')['severity'] }} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {!! session('message')['body'] !!}
        </div>
    </section>
@endif