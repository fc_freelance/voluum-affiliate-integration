@extends('voluum-affiliate::main')

@section('title', 'Offer')

@section('content')

    <section class="va-offer">

        <div class="va-offer-header row">
            @if($offer->isInternal() || $offer->isIntegrated())
                <div class="col-md-9">
                    <h4>Offer {{ $offer->getName() }}</h4>
                </div>
                <div class="va-offer-delete-form col-md-3">
                    <form method='post' action="{{ action('\VoluumAffiliate\Http\Controllers\OffersController@delete', [
                        'id' => $offer->getId(),
                        'redirect_url' => $redirect_url
                    ]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="affiliate_network_internal_id" value="{{ $offer->getInternalNetworkId() }}">
                        <input type="hidden" name="affiliate_network_id" value="{{ $offer->getAffiliateNetworkId() }}">
                        <input type="hidden" name="internal_id" value="{{ $offer->getInternalId() }}">
                        <input type="hidden" name="name" value="{{ $offer->getName() }}">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
            @else
                <div class="col-md-9">
                    <h4>New Offer</h4>
                </div>
            @endif
        </div>

        <form method='post' enctype="multipart/form-data" class="row"
              action="{{ action('\VoluumAffiliate\Http\Controllers\OffersController@'.($offer->isIntegrated() ? 'update' : 'add'), [
                'redirect_url' => $redirect_url
        ])}}">

            <div class="form-group col-md-6">
                <label for="name">Name *</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ $offer->getComposedName()}}" required>
            </div>

            <div class="form-group col-md-6">
                <label for="affiliate_network_id">Affiliate Network *</label>
                <select name="affiliate_network_id" id="affiliate_network_id" class="form-control" required @if($offer->isExternal()) disabled @endif>
                    <option value=""></option>
                    @foreach ($networks as $network)
                        @if ($offer->isExternal() || !$network->hasAPI())
                            <option value="{{ $network->getId() }}" @if($selected_network && $selected_network->getId() === $network->getId()) selected @endif>
                                {{ $network->getName() }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="preview_image">
                    Preview Image
                    <img class="va-offer-preview-image" src="{{ $offer->getPreviewImage() }}" alt="{{ $offer->getPreviewImage() }}">
                </label>
                <input type="file" name="preview_image_file" class="form-control" id="preview_image" placeholder="Preview Image">
            </div>

            <div class="form-group col-md-6">
                <label for="creatives_url">Creatives File <span>{{ $offer->getCreativesUrl() }}</span></label>
                <input type="file" name="creatives_file" class="form-control" id="creatives_url" placeholder="Creatives file">
            </div>

            <div class="form-group col-md-6">
                <label for="preview_url">Preview Url</label>
                <input type="url" name="preview_url" class="form-control" id="preview_url" placeholder="Preview Url" value="{{ $offer->getPreviewUrl() }}">
            </div>

            <div class="form-group col-md-6">
                <label for="country">Country *</label>
                <select name="country" id="country" class="form-control" required>
                    <option value="All">All</option>
                    @foreach ($offer->getCountries() as $country)
                        <option value="{{ $country }}" @if($offer->getCountry() === $country) selected @endif>
                            {{ \VoluumAffiliate\utils\Countries::getNameByCode($country) }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="platforms">Platforms *</label>
                <select name="platforms[]" id="platforms" class="form-control" multiple required @if($offer->isExternal()) disabled @endif>
                    @foreach (\VoluumAffiliate\utils\Util::$platforms as $platform)
                        <option value="{{ $platform }}" @if($offer->getPlatforms() && in_array($platform, $offer->getPlatforms())) selected @endif>
                            {{ $platform }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="cost_model">Cost Model *</label>
                <select name="cost_model" id="cost_model" class="form-control" required @if($offer->isExternal()) disabled @endif>
                    <option value=""></option>
                    @foreach (\VoluumAffiliate\utils\Util::$cost_models as $cm)
                        <option value="{{ $cm }}" @if(strtoupper($offer->getCostModel()) == $cm) selected @endif>
                            {{ $cm }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="payout">Payout *</label>
                <input type="number" step="0.001" name="payout" class="form-control" id="payout" placeholder="Payout"
                       value="{{ $offer->getPayout() }}" required  @if($offer->isExternal()) disabled @endif>
            </div>

            <div class="form-group col-md-6">
                <label for="url">Tracking Url *</label>
                <input type="url" name="url" class="form-control" id="url" placeholder="Tracking Url"
                       value="{{ $offer->getUrl() }}" required  @if($offer->isExternal()) disabled @endif>
            </div>

            <div class="form-group col-md-6">
                <label for="caps">Daily Caps</label>
                <input type="number" name="caps" class="form-control" id="caps" placeholder="Daily Caps"
                       value="{{ $offer->getCaps() }}" @if($offer->isExternal()) disabled @endif>
            </div>

            <div class="col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="incent" value="1" @if($offer->isIncent()) checked @endif @if($offer->isExternal()) disabled @endif>
                        Incent
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="operator_based" value="1" @if($offer->isOperatorBased()) checked @endif @if($offer->isExternal()) disabled @endif>
                        Operator Constraint
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="auto" value="1" @if($offer->isAuto()) checked @endif>
                        Auto
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="auto_payout" value="1" @if($offer->isAutoPayout()) checked @endif>
                        Auto Payout
                    </label>
                </div>
            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="preview_image" value="{{ $offer->getPreviewImage() }}">
            <input type="hidden" name="creatives_url" value="{{ $offer->getCreativesUrl() }}">

            @if($offer->isIntegrated())
                <input type="hidden" name="id" value="{{ $offer->getId() }}">
                <input type="hidden" name="_method" value="put">
            @endif

            @if($offer->isInternal())
                <input type="hidden" name="internal_id" value="{{ $offer->getInternalId() }}">
                <input type="hidden" name="affiliate_network_internal_id" value="{{ $offer->getInternalNetworkId() }}">
            @endif

            @if($offer->isExternal())
                <input type="hidden" name="external_id" value="{{ $offer->getExternalId() }}">
                <input type="hidden" name="affiliate_network_id" value="{{ $offer->getAffiliateNetworkId() }}">
                <input type="hidden" name="source_name" value="{{ $offer->getSourceName() ?: $offer->getName() }}">
                <input type="hidden" name="cost_model" value="{{ $offer->getCostModel() }}">
                <input type="hidden" name="payout" value="{{ $offer->getPayout() }}">
                <input type="hidden" name="url" value="{{ $offer->getUrl() }}">
                <input type="hidden" name="caps" value="{{ $offer->getCaps() }}">
                <input type="hidden" name="incent" value="{{ $offer->isIncent() }}">
                <input type="hidden" name="operator_based" value="{{ $offer->isOperatorBased() }}">
                @if($offer->getPlatforms())
                    @foreach ($offer->getPlatforms() as $platform)
                        <input type="hidden" name="platforms[]" value="{{ $platform }}">
                    @endforeach
                @endif
            @endif

<div class="va-offer-save-form col-md-6">
    <input type="submit" class="btn btn-{{ $offer->isIntegrated() ? 'success' : 'primary' }}" value="{{ $offer->isIntegrated() ? 'Update' : 'Add' }}">
</div>
</form>

</section>
@endsection