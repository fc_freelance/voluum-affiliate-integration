@extends('voluum-affiliate::main')

@section('title', 'Offers')

@section('content')

    <section class="va-filters va-offers-filters row">

        @if (isset($networks) && count($networks))

            <div class="form-group col-sm-7 col-md-4">
                <label for="affiliate_network_id">Affiliate Network</label>
                <select name="affiliate_network_id" id="affiliate_network_id" class="form-control" onchange="window.location.href = '{{ URL::action('\VoluumAffiliate\Http\Controllers\OffersController@show') }}?affiliate_network_id=' + this.value">
                    <option value=""></option>
                    @foreach ($networks as $network)
                        <option value="{{ $network->getId() }}" @if(isset($selected_network) && ($selected_network->getId() === $network->getId())) selected @endif>
                            {{ $network->getName() }}
                        </option>
                    @endforeach
                </select>
            </div>

            @if (isset($selected_network) && !empty($selected_network))

                <div class="form-group col-sm-5 col-md-3 text-left">
                    <label>&nbsp</label>
                    <form method='get' action="{{ action('\VoluumAffiliate\Http\Controllers\OffersController@show') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="affiliate_network_id" value="{{ $selected_network->getId() }}">
                        <input type="hidden" name="page" value="{{ request()->query('page', 1) }}">
                        <input type="hidden" name="items_per_page" value="{{ request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE) }}">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Offer name?">
                            <span class="input-group-addon" style="padding: 0 5px;">
                                <button type="submit" class="btn-link" style="padding: 0;">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>

                    </form>
                </div>

                <div class="form-group col-xs-6 col-md-2 text-left">
                    <label>&nbsp</label>
                    <form method='post' action="{{ action('\VoluumAffiliate\Http\Controllers\OffersController@updateAllFromNetwork', [
                        'affiliate_network_id' => $selected_network->getId(),
                        'page' => request()->query('page', 1),
                        'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE)
                    ]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="affiliate_network_internal_id" value="{{ $selected_network->getInternalId() }}">
                        <input type="submit" class="btn btn-primary" value="Update Offers">
                    </form>
                </div>
            @endif

            <div class="form-group col-xs-6 col-md-2 {{ (isset($selected_network) && !empty($selected_network)) ? 'col-md-offset-1 text-right' : '' }}">
                <label>&nbsp</label>
                <div>
                    <a class="btn btn-success"
                       href="{{ URL::action('\VoluumAffiliate\Http\Controllers\OffersController@showSingleNew', [
                            'redirect_url' => Request::fullUrl()
                       ]) }}">
                        New Offer
                    </a>
                </div>
            </div>

        @else

            <div class="center-block text-center">
                No Affiliate Networks found!!!
            </div>

        @endif

    </section>

    @if (isset($selected_network) && !empty($selected_network))

        <section class="va-offers-controls row">
            @if (isset($pagination) && $pagination['pages_count'] > 1)
                <div class="va-pagination col-12 col-md-offset-6 col-md-6 col-lg-offset-8 col-lg-4 text-right">
                    <ul class="pagination">
                        <li class="{{ $pagination['page'] < 2 ? 'disabled' : ''}}">
                            <a aria-label="First" href={{ action('\VoluumAffiliate\Http\Controllers\OffersController@show', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => 1,
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}>
                                <span aria-hidden="true">&laquo;&laquo;</span>
                            </a>
                        </li>
                        <li class="{{ $pagination['page'] < 2 ? 'disabled' : ''}}">
                            <a aria-label="Previous" href={{ action('\VoluumAffiliate\Http\Controllers\OffersController@show', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => $pagination['page'] - 1,
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        @for ($i = $pagination['page_min']; $i <= $pagination['page_max']; $i++)
                            <li class="{{ $i == $pagination['page'] ? 'active' : '' }}">
                                <a href={{ action('\VoluumAffiliate\Http\Controllers\OffersController@show', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => $i,
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}>
                                    {{ $i }}
                                </a>
                            </li>
                        @endfor
                        <li class="{{ $pagination['page'] >= $pagination['pages_count'] ? 'disabled' : ''}}">
                            <a aria-label="Next" href={{ action('\VoluumAffiliate\Http\Controllers\OffersController@show', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => $pagination['page'] + 1,
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li class="{{ $pagination['page'] >= $pagination['pages_count'] ? 'disabled' : ''}}">
                            <a aria-label="Last" href={{ action('\VoluumAffiliate\Http\Controllers\OffersController@show', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => $pagination['pages_count'],
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}>
                                <span aria-hidden="true">&raquo;&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </div>
            @endif
        </section>

        <section class="va-offers table-responsive">

            @if (isset($offers) && count($offers))
                <table class="table table-condensed table-striped va-table va-offers-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            @if($selected_network->hasApi())
                                <th>Source Name</th>
                            @endif
                            <th>Platforms</th>
                            <th>Country</th>
                            <th>Cost Model</th>
                            <th>Payout</th>
                            <th>Daily Caps</th>
                            <th>Incent</th>
                            <th>Auto</th>
                            <th>Payout Auto</th>
                            <th>Url</th>
                            <th>Creatives Url</th>
                            <th>
                                <form method='post' action="{{ action('\VoluumAffiliate\Http\Controllers\OffersController@addNetworkOffers', [
                                    'affiliate_network_id' => $selected_network->getId(),
                                    'page' => request()->query('page', 1),
                                    'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE),
                                    'search' => request()->query('search', '')
                                ]) }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-primary" value="Add All">
                                </form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($offers as $offer)
                            <tr>
                                <td class="va-offer-cell-preview-image">
                                    @if($offer->getPreviewImage())
                                        <img src="{{ $offer->getPreviewImage() }}" alt="{{ $offer->getSourceName() }}"/>
                                    @endif
                                    @if($offer->getPreviewUrl())
                                        <a href="{{ $offer->getPreviewUrl() }}" target="_blank">Preview</a>
                                    @else
                                        <div>No Preview</div>
                                    @endif
                                </td>

                                <td class="va-offer-cell-name">{{ $offer->getComposedName() }}</td>

                                @if($offer->isExternal())
                                    <td class="va-offer-cell-source-name">{{ $offer->getSourceName() }}</td>
                                @endif

                                <td class="va-offer-cell-platforms">{{ $offer->getPlatforms() ? join(", ", $offer->getPlatforms()) : '' }}</td>

                                <td class="va-offer-cell-country">{{ $offer->getCountry() ? \VoluumAffiliate\utils\Countries::getNameByCode($offer->getCountry()) : 'All' }}</td>

                                <td class="va-offer-cell-cost_model">{{ $offer->getCostModel() }}</td>

                                <td class="va-offer-cell-payout">{{ $offer->getPayout() }}</td>

                                <td class="va-offer-cell-caps">{{ $offer->getCaps() }}</td>

                                <td class="va-offer-cell-incent">{{ $offer->isIncent() ? 'Yes' : 'No' }}</td>

                                <td class="va-offer-cell-auto">{{ $offer->isAuto() ? 'Yes' : 'No' }}</td>

                                <td class="va-offer-cell-auto_payout">{{ $offer->isAutoPayout() ? 'Yes' : 'No' }}</td>

                                <td class="va-offer-cell-url">{{ $offer->getUrl() }}</td>

                                <td class="va-offer-cell-creatives_url">{{ $offer->getCreativesUrl() }}</td>

                                <td class="va-offer-cell-add">
                                    <a class="btn btn-{{ $offer->isIntegrated() ? 'warning' : 'primary' }}"
                                           href="{{ URL::action('\VoluumAffiliate\Http\Controllers\OffersController@showSingle'.($offer->isIntegrated() ? 'Integrated' : ($offer->isInternal() ? 'Internal' : ($offer->isExternal() ? 'External' : ''))), [
                                                'id' => $offer->getId(),
                                                'internal_id' => $offer->getInternalId(),
                                                'external_id' => $offer->getExternalId(),
                                                'network_id' => $offer->getAffiliateNetworkId(),
                                                'redirect_url' => Request::fullUrl(),
                                                'page' => request()->query('page', 1),
                                                'items_per_page' => request()->query('items_per_page', \VoluumAffiliate\networks\NetworksManager::ITEMS_PER_PAGE)
                                           ]) }}">
                                        {{ $offer->isIntegrated() ? 'Edit' : 'Add' }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="center-block text-center">
                    No Offers found!!!
                </div>
            @endif

        </section>

    @else
        <div class="center-block text-center">
            <h3><strong>Select an affiliate network in order to browse its offers</strong></h3>
        </div>
    @endif
@endsection