<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ request()->path() === 'admin/affiliate-networks' ? 'active' : '' }}">
                    <a href="{{ URL::action('\VoluumAffiliate\Http\Controllers\AffiliateNetworksController@show') }}">
                        Affiliate Networks <span class="sr-only">{{ request()->path() === 'admin/affiliate-networks' ? '(current)' : '' }}</span>
                    </a>
                </li>
                <li class="{{ request()->path() === 'admin/traffic-sources' ? 'active' : '' }}">
                    <a href="{{ URL::action('\VoluumAffiliate\Http\Controllers\TrafficSourcesController@show') }}">
                        Traffic Sources <span class="sr-only">{{ request()->path() === 'admin/traffic-sources' ? '(current)' : '' }}</span>
                    </a>
                </li>
                <li class="{{ request()->path() === 'admin/offers' ? 'active' : '' }}">
                    <a href="{{ URL::action('\VoluumAffiliate\Http\Controllers\OffersController@show') }}">
                        Offers <span class="sr-only">{{ request()->path() === 'admin/offers' ? '(current)' : '' }}</span>
                    </a>
                </li>
                <li class="{{ request()->path() === 'admin/reports/live/campaigns' ? 'active' : '' }}">
                    <a href="{{ URL::action('\VoluumAffiliate\Http\Controllers\CampaignsController@showLiveReport') }}">
                        Live Report <span class="sr-only">{{ request()->path() === 'admin/reports/live/campaigns' ? '(current)' : '' }}</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::action('Auth\AuthController@getLogout') }}">Logout</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
