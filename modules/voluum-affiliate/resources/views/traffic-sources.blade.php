@extends('voluum-affiliate::main')

@section('title', 'Traffic Sources')

@section('content')

    <section class="va-traffic-sources table-responsive">

        @if (isset($sources) && count($sources))
            <table class="table table-condensed table-striped va-table va-traffic-sources-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Countries</th>
                    <th>Platforms</th>
                    <th>Cost Model</th>
                    <th>Min Daily Caps</th>
                    <th>Incent</th>
                    <th>Auto</th>
                    <th>Auto Payout</th>
                    <th>Postback Url</th>
                    <th>API Endpoint</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($sources as $source)
                    <tr>
                        <form method='post'
                              action="{{ action('\VoluumAffiliate\Http\Controllers\TrafficSourcesController@'.(empty($source->getId()) ? 'add' : 'update'), [
                            'id' => $source->getId()
                        ]) }}">

                            <td class="va-traffic-source-cell-name">
                                <input type="text" name="name" placeholder="Name" value="{{ $source->getName() }}" required class="form-control" disabled>
                            </td>

                            <td class="va-traffic-source-cell-countries">
                                <select name="countries[]" id="countries" class="form-control" multiple>
                                    @foreach (\VoluumAffiliate\utils\Countries::all() as $country)
                                        <option value="{{ $country['code'] }}" @if($source->getCountries() && in_array($country['code'], $source->getCountries())) selected @endif>
                                            {{ $country['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </td>

                            <td class="va-traffic-source-cell-platforms">
                                <select name="platforms[]" id="platforms" class="form-control" multiple required>
                                    @foreach (\VoluumAffiliate\utils\Util::$platforms as $platform)
                                        <option value="{{ $platform }}" @if($source->getPlatforms() && in_array($platform, $source->getPlatforms())) selected @endif>
                                            {{ $platform }}
                                        </option>
                                    @endforeach
                                </select>
                            </td>

                            <td class="va-traffic-source-cell-cost_model">
                                <select name="cost_model" id="cost_model" class="form-control" required>
                                    <option value=""></option>
                                    @foreach (\VoluumAffiliate\utils\Util::$cost_models as $cm)
                                        <option value="{{ $cm }}" @if($source->getCostModel() && strtoupper($source->getCostModel()) == $cm) selected @endif>
                                            {{ $cm }}
                                        </option>
                                    @endforeach
                                </select>
                            </td>

                            <td class="va-traffic-source-cell-min_caps">
                                <input type="number" name="min_caps" class="form-control" id="min_caps" placeholder="Min Daily Caps" value="{{ $source->getMinCaps() }}">
                            </td>

                            <td class="va-traffic-source-cell-incent">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="incent" value="1" @if($source->isIncent()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-traffic-source-cell-auto">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="auto" value="1" @if($source->isAuto()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-traffic-source-cell-auto_payout">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="auto_payout" value="1" @if($source->isAutoPayout()) checked @endif>
                                    </label>
                                </div>
                            </td>

                            <td class="va-traffic-source-cell-postback_url">
                                <input type="url" name="postback_url" placeholder="Postback Url" value="{{ $source->getPostbackUrl() }}" disabled class="form-control">
                            </td>

                            <td class="va-traffic-source-cell-internal-api-endpoint">
                                <input type="text" name="api_endpoint" placeholder="API Endpoint" value="{{ $source->getApiEndpoint() }}" disabled class="form-control">
                            </td>

                            <td class="va-traffic-source-cell-add">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="internal_id" value="{{ $source->getInternalId() }}">
                                <input type="hidden" name="name" value="{{ $source->getName() }}">
                                <input type="hidden" name="postback_url" value="{{ $source->getPostbackUrl() }}">
                                @if(!empty($source->getId()))
                                    <input type="hidden" name="id" value="{{ $source->getId() }}">
                                    <input type="hidden" name="_method" value="put">
                                @endif
                                <input type="submit" class="btn btn-{{ empty($source->getId()) ? 'primary' : 'warning' }}"
                                       value="{{ empty($source->getId()) ? 'Add' : 'Update' }}">
                            </td>

                        </form>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="center-block text-center">
                No Traffic sources found!!!
            </div>
        @endif

    </section>

@endsection