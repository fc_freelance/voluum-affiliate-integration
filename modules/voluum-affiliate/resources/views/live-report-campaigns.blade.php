@extends('voluum-affiliate::main')

@section('title', 'Live report - Campaigns')

@section('content')

    <section class="va-filters va-campaign-filters row">

        @if (isset($campaigns) && count($campaigns))

            <div class="form-group col-sm-10 col-md-4">
                <label for="campaign_id">Campaign</label>
                <select name="campaign_id" id="campaign_id" class="form-control" onchange="window.location.href = '{{ URL::action('\VoluumAffiliate\Http\Controllers\CampaignsController@showLiveReport') }}?campaign_id=' + this.value">
                    @foreach ($campaigns as $campaign)
                        <option value="{{ $campaign->campaignId }}" @if($selected_campaign_id === $campaign->campaignId) selected @endif>
                            {{ $campaign->campaignName }}
                        </option>
                    @endforeach
                </select>
            </div>

        @else

            <div class="center-block text-center">
                No Campaigns found!!!
            </div>

        @endif

    </section>

    <section class="va-live-report table-responsive">

        @if (isset($report) && count($report))
            <table class="table table-condensed table-striped va-table va-offers-table">
                <thead>
                    <tr>
                        <th>Timestamp</th>
                        <th>Country</th>
                        <th>Region</th>
                        <th>City</th>
                        <th>Offer</th>
                        <th>IP</th>
                        <th>External Id</th>
                        <th>V1</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($report as $entry)
                        <tr>
                            <td class="va-report-cell-timestamp">{{ $entry->timestamp }}</td>
                            <td class="va-report-cell-country">{{ $entry->countryName }}</td>
                            <td class="va-report-cell-country">{{ $entry->region }}</td>
                            <td class="va-report-cell-country">{{ $entry->city }}</td>
                            <td class="va-report-cell-country">{{ $entry->offerName }}</td>
                            <td class="va-report-cell-country">{{ $entry->ip }}</td>
                            <td class="va-report-cell-country">{{ $entry->externalId }}</td>
                            <td class="va-report-cell-country">{{ $entry->customVariable1 }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="center-block text-center">
                No entries found!!!
            </div>
        @endif

    </section>
@endsection