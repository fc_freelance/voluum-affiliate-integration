<?php

namespace VoluumAffiliate\Services;

use stdClass;
use VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository;
use VoluumAffiliate\Database\Repositories\IOffersRepository;
use VoluumAffiliate\Database\Repositories\ITrafficSourcesRepository;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Networks\IInternalNetworkApi;
use VoluumAffiliate\Networks\INetworkApi;
use VoluumAffiliate\networks\NetworksManager;
use VoluumAffiliate\utils\Countries;
use VoluumAffiliate\Utils\Util;

/**
 * Class OffersService
 * @package VoluumAffiliate\Services
 */
class OffersService {

    /** @var IAffiliateNetworksRepository $networksRepo */
    protected $networksRepo;

    /** @var ITrafficSourcesRepository $sourcesRepo */
    protected $sourcesRepo;

    /** @var IOffersRepository $offersRepo */
    protected $offersRepo;

    /** @var IInternalNetworkApi $internalApi */
    protected $internalApi;

    /** @var CampaignsService $campaignsService */
    protected $campaignsService;

    /** @var AffiliateNetworksService $affiliateNetworksService */
    protected $affiliateNetworksService;

    /**
     * @param IAffiliateNetworksRepository $networksRepo
     * @param ITrafficSourcesRepository $sourcesRepo
     * @param IOffersRepository $offersRepo
     * @param IInternalNetworkApi $internalApi
     * @param CampaignsService $campaignsService
     * @param AffiliateNetworksService $affiliateNetworksService
     */
    public function __construct(IAffiliateNetworksRepository $networksRepo, ITrafficSourcesRepository $sourcesRepo, IOffersRepository $offersRepo,
                                IInternalNetworkApi $internalApi, CampaignsService $campaignsService, AffiliateNetworksService $affiliateNetworksService)
    {
        $this->internalApi = $internalApi;
        $this->networksRepo = $networksRepo;
        $this->sourcesRepo = $sourcesRepo;
        $this->offersRepo = $offersRepo;
        $this->campaignsService = $campaignsService;
        $this->affiliateNetworksService = $affiliateNetworksService;
    }

    /**
     * @param $network
     * @param $data
     * @return array
     */
    public function getNetworkOffers($network, $data = array())
    {
        // Get network API
        /** @var INetworkApi $api */
        $api = NetworksManager::getNetworkApi($network);

        // No API, no data
        if (empty($api)) return array();

        // Get offers and parse them
        return $api->payloadToOffers($api->listOffers($data));
    }

    /**
     * @param $network
     * @param $data
     * @return array
     */
    public function getRawNetworkOffers($network, $data = array())
    {
        // Get network API
        /** @var INetworkApi $api */
        $api = NetworksManager::getNetworkApi($network);

        // No API, no data
        if (empty($api)) return array();

        // Get offers and return them
        return $api->listOffers($data);
    }

    /**
     * @param $network
     * @param $id
     * @return mixed
     */
    public function getNetworkOffer($network, $id = '')
    {
        // Get network API
        /** @var INetworkApi $api */
        $api = NetworksManager::getNetworkApi($network);

        // No API, no data
        if (empty($api)) return null;

        // Get offers and parse them
        return $api->payloadToOffer($api->getOffer($id));
    }

    /**
     * @param $network
     * @param string $id
     * @return mixed
     */
    public function getRawNetworkOffer($network, $id = '')
    {
        // Get network API
        /** @var INetworkApi $api */
        $api = NetworksManager::getNetworkApi($network);

        // No API, no data
        if (empty($api)) return null;

        // Get offer and return it
        return $api->getOffer($id);
    }

    /**
     * @param string|integer $network_id
     * @param string|integer $id
     * @param array $data
     * @return null|Offer
     */
    public function getAffiliateNetworkOffer($network_id = '', $id = '', $data = array())
    {
        // Check if empty params
        if (empty($network_id) || empty($id)) return null;

        // Get network
        $network = $this->affiliateNetworksService->get($network_id);

        // Check if empty
        if (empty($network)) return null;

        // Create object
        $network = new AffiliateNetwork($network);

        // Check if code
        if (empty($code = $network->getNetworkCode()) || $code == '_none') return null;

        // Get network API
        /** @var INetworkApi $api */
        $api = NetworksManager::getNetworkApi($code);

        // No API, no data
        if (empty($api)) return null;

        // Get offer
        /** Offer $offer */
        $offer = $api->getOffer($id, $data);

        // Autoselect if only one country available
        if (count($countries = $offer->getCountries()) == 1) {
            $offer->setCountry($countries[0]);
        }

        // Let's set the affiliate network id anyway
        $offer->setAffiliateNetworkId($network->getId());

        // Return offer
        return $offer;
    }

    /**
     * @param string $id
     * @return array
     */
    public function getInternalOffer($id = '')
    {
        return $this->internalApi->payloadToOffer($this->getRawInternalOffer($id));
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getRawInternalOffer($id = '')
    {
        return $this->internalApi->getOffer($id);
    }

    /**
     * @return array
     */
    public function getInternalOffers()
    {
        return $this->internalApi->payloadToOffers($this->getRawInternalOffers());
    }

    /**
     * @return array
     */
    public function getRawInternalOffers()
    {
        return $this->internalApi->listOffers();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getIntegratedOffers($data = array())
    {
        // Get offers as from repository
        return $this->offersRepo->query($data);
    }

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function getIntegratedOffer($id = '')
    {
        // Get offers as from repository
        return $this->offersRepo->get($id);
    }

    /**
     * @param array $data
     * @return string
     */
    public function getOfferComposedNameFromFormData($data)
    {
        // If name is set, return it
        if (isset($data['name'])) return $data['name'];

        // Compose and return it
        $name = isset($data['source_name']) ? $data['source_name'] : $data['external_id'];
        $name .= (isset($data['platforms']) && !empty($data['platforms'])) ? ('_'.join(',',$data['platforms'])) : '';
        $name .= (isset($data['cost_model']) && !empty($data['cost_model'])) ? ('_'.$data['cost_model']) : '';
        $name .= (isset($data['incent']) && $data['incent']) ? '_Incent' : '';
        return $name;
    }

    /**
     * @param $data
     * @return array
     */
    public function dbPayloadToOffers($data)
    {
        // Init collection
        $parsed = array();

        // Loop and create objects
        foreach($data as $entry) {
            $parsed[] = $this->dbPayloadToOffer($entry);
        }

        // Return parsed collection
        return $parsed;
    }

    /**
     * @param stdClass $data
     * @return null|Offer
     */
    public function dbPayloadToOffer($data)
    {
        // Check
        if (!isset($data) || empty($data)) return null;

        // Parse platforms
        if (!empty($platforms = $data->platforms)) {
            $platforms = explode(',', $platforms);
        }

        // Parse affiliate network internal id
        if (!empty($affiliate_network_id = $data->affiliate_network_id)) {
            if (!empty($afNetwork = $this->affiliateNetworksService->get($affiliate_network_id))) {
                $internal_network = new AffiliateNetwork($afNetwork);
            }
        }

        // Return parsed collection
        return new Offer(array(
            'id' => $data->id,
            'internal_id' => $data->internal_id,
            'external_id' => $data->external_id,
            'internal_network_id' => isset($internal_network) ? $internal_network->getInternalId() : null,
            'affiliate_network_id' => $affiliate_network_id,
            'preview_url' => $data->preview_url,
            'preview_image' => $data->preview_image,
            'platforms' => $platforms,
            'caps' => $data->caps,
            'cost_model' => $data->cost_model,
            'incent' => $data->incent,
            'operator_based' => $data->operator_based,
            'creatives_url' => $data->creatives_url,
            'auto' => $data->auto,
            'auto_payout' => $data->auto_payout
        ));
    }

    /**
     * @param Offer $offer
     * @return null|array
     */
    public function offerToDbPayload($offer)
    {
        // Check
        if (!isset($offer) || empty($offer)) return null;

        // Parse platforms
        if (!empty($platforms = $offer->getPlatforms())) {
            $platforms = join(',', $platforms);
        }

        // Parse affiliate network, wither from direct id or from internal id
        if (empty($affiliate_network_id = $offer->getAffiliateNetworkId())) {
            if (!empty($ani_id = $offer->getInternalNetworkId()) &&
                    !empty($afNetwork = $this->affiliateNetworksService->find(array('internal_id' => $ani_id)))) {
                $internal_network = new AffiliateNetwork($afNetwork);
                $affiliate_network_id = $internal_network->getId();
            }
        }

        // Create offer mappings for DB
        return array(
            'id' => $offer->getId(),
            'internal_id' => $offer->getInternalId(),
            'external_id' => $offer->getExternalId(),
            'affiliate_network_id' => $affiliate_network_id,
            'preview_url' => $offer->getPreviewUrl(),
            'preview_image' => $offer->getPreviewImage(),
            'platforms' => $platforms,
            'caps' => $offer->getCaps(),
            'cost_model' => $offer->getCostModel(),
            'incent' => $offer->isIncent(),
            'operator_based' => $offer->isOperatorBased(),
            'creatives_url' => $offer->getCreativesUrl(),
            'auto' => $offer->isAuto(),
            'auto_payout' => $offer->isAutoPayout()
        );
    }

    /**
     * @param array $data
     * @return Offer
     */
    public function offerFormToOffer($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Form the name
        $name = $this->getOfferComposedNameFromFormData($data);

        // Get proper data from input
        $parsedData = array(
            'payout' => $data['payout'],
            'name' => $name,
            'url' => $data['url']
        );

        // If set, assign DB id
        if (isset($data['id'])) {
            $parsedData['id'] = $data['id'];
        }

        // If set, assign internal id
        if (isset($data['internal_id'])) {
            $parsedData['internal_id'] = $data['internal_id'];
        }

        // If set, assign external id
        if (isset($data['external_id'])) {
            $parsedData['external_id'] = $data['external_id'];
        }

        // If set, assign external id
        if (isset($data['affiliate_network_id'])) {
            $parsedData['affiliate_network_id'] = $data['affiliate_network_id'];
        }

        // If set, assign external id
        if (isset($data['affiliate_network_internal_id'])) {
            $parsedData['internal_network_id'] = $data['affiliate_network_internal_id'];
        }

        // Check country
        if (isset($data['country']) && $data['country'] !== 'All') {
            $parsedData['country'] = $data['country'];
        }

        // Check source_name
        if (isset($data['source_name'])) {
            $parsedData['source_name'] = $data['source_name'];
        }

        // Check platforms
        if (isset($data['platforms']) && !empty($data['platforms'])) {
            $parsedData['platforms'] = $data['platforms'];
        }

        // Check available countries
        if (isset($data['countries']) && !empty($data['countries'])) {
            $parsedData['countries'] = $data['countries'];
        }

        // Check preview url
        if (isset($data['preview_url'])) {
            $parsedData['preview_url'] = $data['preview_url'];
        }

        // Check preview image
        if (isset($data['preview_image'])) {
            $parsedData['preview_image'] = $data['preview_image'];
        }

        // Check caps
        if (isset($data['caps'])) {
            $parsedData['caps'] = $data['caps'];
        }

        // Check cost model
        if (isset($data['cost_model'])) {
            $parsedData['cost_model'] = $data['cost_model'];
        }

        // Check incent
        if (isset($data['incent'])) {
            $parsedData['incent'] = $data['incent'];
        }

        // Check operator
        if (isset($data['operator_based'])) {
            $parsedData['operator_based'] = $data['operator_based'];
        }

        // Check preview image
        if (isset($data['creatives_url'])) {
            $parsedData['creatives_url'] = $data['creatives_url'];
        }

        // Check auto
        if (isset($data['auto'])) {
            $parsedData['auto'] = $data['auto'];
        }

        // Check auto-payout
        if (isset($data['auto_payout'])) {
            $parsedData['auto_payout'] = $data['auto_payout'];
        }

        // Fix affiliate network internal id if not set
        if (empty($parsedData['internal_network_id']) && isset($parsedData['affiliate_network_id'])) {
            /** @var AffiliateNetwork $network */
            $network = $this->affiliateNetworksService->get($parsedData['affiliate_network_id']);
            if (!empty($network)) {
                $network = new AffiliateNetwork($network);
                $parsedData['internal_network_id'] = $network->getInternalId();
            }
        }

        // Create Offer object with fixed data
        return new Offer($parsedData);
    }

    /**
     * @param Offer|null $iOffer
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function mergeInternalOfferWithOffer($iOffer, $offer)
    {
        return $this->internalApi->mergeInternalOfferWithOffer($iOffer, $offer);
    }

    /**
     * @param Offer|null $dbOffer
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function mergeDbOfferWithOffer($dbOffer, $offer)
    {
        // No offer, return just internal
        if (empty($offer)) return $dbOffer;

        // Check if got internal
        if (!empty($dbOffer)) {

            /** ONLY SET IF NOT YET PROPERTIES */
            if (empty($offer->getPlatforms()) &&  $platforms = $dbOffer->getPlatforms()) {
                $offer->setPlatforms($platforms);
            }

            if (empty($offer->getCountries()) && $countries = $dbOffer->getCountries()) {
                $offer->setCountries($countries);
            }

            if (empty($offer->getCostModel()) && $cost_model = $dbOffer->getCostModel()) {
                $offer->setCostModel($cost_model);
            }

            if (empty($offer->getCaps()) && $caps = $dbOffer->getCaps()) {
                $offer->setCaps($caps);
            }

            /** ALWAYS SET FROM DB PROPERTIES */
            if ($id = $dbOffer->getId()) {
                $offer->setId($id);
            }

            if ($iafNetId = $dbOffer->getInternalNetworkId()) {
                $offer->setInternalNetworkId($iafNetId);
            }

            if ($aNetId = $dbOffer->getAffiliateNetworkId()) {
                $offer->setAffiliateNetworkId($aNetId);
            }

            if ($internalId = $dbOffer->getInternalId()) {
                $offer->setInternalId($internalId);
            }

            if ($externalId = $dbOffer->getExternalId()) {
                $offer->setExternalId($externalId);
            }

            if ($img_preview = $dbOffer->getPreviewImage()) {
                $offer->setPreviewImage($img_preview);
            }

            if ($url_preview = $dbOffer->getPreviewUrl()) {
                $offer->setPreviewUrl($url_preview);
            }

            if ($url_creatives = $dbOffer->getCreativesUrl()) {
                $offer->setCreativesUrl($url_creatives);
            }

            if ($incent = $dbOffer->isIncent()) {
                $offer->setIncent($incent);
            }

            if ($operator = $dbOffer->isOperatorBased()) {
                $offer->setOperatorBased($operator);
            }

            if ($auto = $dbOffer->isAuto()) {
                $offer->setAuto($auto);
            }

            if ($auto_payout = $dbOffer->isAutoPayout()) {
                $offer->setAutoPayout($auto_payout);
            }
        }

        // Return the offer
        return $offer;
    }

    /**
     * @param Offer|null $afOffer
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function mergeExternalOfferWithOffer($afOffer, $offer)
    {
        // No offer, return just internal
        if (empty($offer)) return $afOffer;

        // Check if got internal
        if (!empty($afOffer)) {

            /** ONLY SET IF NOT YET PROPERTIES */
            if (empty($offer->getInternalNetworkId()) && $iNetId = $afOffer->getInternalNetworkId()) {
                $offer->setInternalNetworkId($iNetId);
            }

            if (empty($offer->getPreviewImage()) && $img_preview = $afOffer->getPreviewImage()) {
                $offer->setPreviewImage($img_preview);
            }

            if (empty($offer->getPreviewUrl()) && $url_preview = $afOffer->getPreviewUrl()) {
                $offer->setPreviewUrl($url_preview);
            }

            if (empty($offer->getCreativesUrl()) && $url_creatives = $afOffer->getCreativesUrl()) {
                $offer->setCreativesUrl($url_creatives);
            }

            if ($offer->isIncent() == null && $incent = $afOffer->isIncent()) {
                $offer->setIncent($incent);
            }

            if ($offer->isOperatorBased() == null && $operator = $afOffer->isOperatorBased()) {
                $offer->setOperatorBased($operator);
            }

            if ($offer->isAuto() == null && $auto = $afOffer->isAuto()) {
                $offer->setAuto($auto);
            }

            if ($offer->isAutoPayout() == null && $auto_payout = $afOffer->isAutoPayout()) {
                $offer->setAutoPayout($auto_payout);
            }

            /** ALWAYS SET FROM AFFILIATE PROPERTIES */
            if ($platforms = $afOffer->getPlatforms()) {
                $offer->setPlatforms($platforms);
            }

            if ($countries = $afOffer->getCountries()) {
                $offer->setCountries($countries);
            }

            if ($country = $afOffer->getCountry()) {
                $offer->setCountry($country);
            }

            if ($cost_model = $afOffer->getCostModel()) {
                $offer->setCostModel($cost_model);
            }

            if ($payout = $afOffer->getPayout()) {
                $offer->setPayout($payout);
            }

            if ($caps = $afOffer->getCaps()) {
                $offer->setCaps($caps);
            }

            if ($url = $afOffer->getUrl()) {
                $offer->setUrl($url);
            }

            if ($source_name = $afOffer->getSourceName()) {
                $offer->setSourceName($source_name);
            }
        }

        // Return the offer
        return $offer;
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportOffersToPayload($data = array())
    {
        return $this->internalApi->reportOffersToPayload($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportOfferToPayload($data = array())
    {
        return $this->internalApi->reportOfferToPayload($data);
    }

    /**
     * @param Offer $offer
     * @return array
     */
    public function addOffer($offer)
    {
        // Missing data, return error
        if (!isset($offer) || empty($offer)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for adding offer'
                )
            );
        }

        // Form the name
        $name = $offer->getComposedName();

        // Check if not internal yet
        if (!$offer->isInternal()) {

            // Time to see if already in Voluum
            $rawInternalOffers = $this->internalApi->getReport(array('group_by' => 'offer'));

            // Parse them
            $internalOffers = $this->reportOffersToPayload($rawInternalOffers->rows);

            // Ok, we need affiliate networks here just for the name
            $afNetwork = $this->affiliateNetworksService->getIntegratedById($offer->getAffiliateNetworkId());

            // Filter them for this one
            $filteredOffers = array_values(array_filter($internalOffers, function($entry) use ($offer, $afNetwork) {
                $countryName = $offer->getCountry() ? Countries::getNameByCode($offer->getCountry()) : $this->internalApi->getConfig('defaults.all_countries_text');
                return $afNetwork->getName() == $entry['affiliateNetworkName'] &&
                    $entry['country'] == Countries::getNameByCode($offer->getCountry()) &&
                    $entry['name'] == ($afNetwork->getName() . ' - '  . $countryName . ' - ' . $offer->getComposedName());
            }));

            // Check if no one
            if (empty($filteredOffers)) {

                // Create offer into Internal Network
                $internal_offer = $this->internalApi->createOffer($this->internalApi->offerToPayload($offer));

                // Default message
                $defaultErrorMessage = "Could not add offer ${name} to Internal Network";

                // Error creating offer into Internal network
                if (empty($internal_offer)) {
                    return array(
                        'error' => array(
                            'code' => 500,
                            'message' => $defaultErrorMessage
                        )
                    );
                }

                // Error creating offer into Internal network
                if (isset($internal_offer->data->error)) {
                    return array(
                        'error' => array(
                            'code' => 500,
                            'message' => "${defaultErrorMessage} : {$internal_offer->data->error->description}"
                        )
                    );
                }

            // It already exists, and also active
            } else if ($filteredOffers[0]['active']) {

                return array(
                    'error' => array(
                        'code' => 500,
                        'message' => "Attempt to create an already existing offer for network {$afNetwork->getName()} with name {$offer->getName()}"
                    )
                );

            // It's archived!
            } else {

                // Try to recover it
                $recoveredOffer = $this->internalApi->recoverOffer($filteredOffers[0]['id']);

                // Check if not properly recovered
                if (empty($recoveredOffer) || $recoveredOffer->status != 204 || (isset($recoveredOffer->data) && isset($recoveredOffer->data->error))) {

                    // End here
                    return array(
                        'error' => array(
                            'code' => 500,
                            'message' => "Failed to recover an already existing but archived offer for network {$afNetwork->getName()} with name {$offer->getName()}"
                        )
                    );

                // Success recovering
                } else {

                    // Set the id
                    $offer->setInternalId($filteredOffers[0]['id']);

                    // Try to update it
                    $internal_offer = $this->internalApi->updateOffer($this->internalApi->offerToPayload($offer));

                    // Check if not properly updated
                    if (empty($internal_offer) || isset($internal_offer->data->error)) {

                        // End here
                        return array(
                            'error' => array(
                                'code' => 500,
                                'message' => "Failed to update a recovered offer for network {$afNetwork->getName()} with name {$offer->getName()}"
                            )
                        );
                    }
                }
            }

            // Parse offer
            $internal_offer = $this->internalApi->payloadToOffer($internal_offer->data);

            // Merge with former one
            $offer = $this->internalApi->mergeInternalOfferWithOffer($internal_offer, $offer);
        }

        // Create offer mappings in DB
        $dbOffer = $this->offersRepo->create($this->offerToDbPayload($offer));

        // Error creating offer in DB
        if (empty($dbOffer) || !$dbOffer) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => "Could not add offer ${name} to DB"
                )
            );
        }

        // Init feedback
        $output = Util::multiLog("Added offer ${name}", 'info');

        // Create relevant campaigns for offer. Check if Auto, otherwise skip
        if ($offer->isAuto()) {

            // TODO: Comment/Uncomment below ensuring offers are properly created
            $output .= $this->campaignsService->createOfferCampaigns($offer);
        }

        // Return success
        return array('success' => true, 'output' => $output);
    }

    /**
     * @param Offer $offer
     * @return array
     */
    public function updateOffer($offer)
    {
        // Missing data, return error
        if (!isset($offer) || empty($offer)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for updating offer'
                )
            );
        }

        // Create offer into Internal Network
        $internal_offer = $this->internalApi->updateOffer($this->internalApi->offerToPayload($offer));

        // Default message
        $defaultErrorMessage = "Could not update offer {$offer->getName()} into Internal Network";

        // Error updating offer into Internal network
        if (empty($internal_offer)) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => $defaultErrorMessage
                )
            );
        }

        // Error updating offer into Internal network
        if (isset($internal_offer->data->error)) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => "${defaultErrorMessage} : {$internal_offer->data->error->description}"
                )
            );
        }


        // Create offer mappings in DB
        $dbOfferUpdated = $this->offersRepo->update($offer->getId(), $this->offerToDbPayload($offer));

        // Error creating offer in DB
        // TODO: Check if this is ok
        if ($dbOfferUpdated === null) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => "Could not update offer {$offer->getName()} into DB"
                )
            );
        }

        // Init feedback
        $output = Util::multiLog("Updated offer {$offer->getName()}", 'info');

        // Update relevant campaigns for offer. Check if Auto, otherwise skip
        if ($offer->isAuto()) {

            // TODO: Comment/Uncomment below ensuring campaigns are properly updated
            $output .= $this->campaignsService->updateOfferCampaigns($offer);

            // TODO: Uncomment this after ensuring campaigns are properly created
            $output .= $this->campaignsService->createOfferCampaigns($offer, true);
        }

        // Return success
        return array('success' => true, 'output' => $output);
    }

    /**
     * @param array $data
     * @return array
     */
    public function deleteOffer($data = array())
    {
        // Missing data, return error
        if (empty($data) || !isset($data['id']) || !isset($data['internal_id'])) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for deleting offer'
                )
            );
        }

        // Get offer from API
        $vOffer = $this->internalApi->payloadToOffer($this->internalApi->getOffer($data['internal_id']));

        // Error getting offer into API
        if (empty($vOffer)) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not find offer in Internal API for deletion'
                )
            );
        }

        // Get offer mappings in DB
        $dbOffer = $this->dbPayloadToOffer($this->offersRepo->get($data['id']));

        // Error creating offer in DB
        if (empty($dbOffer) || !$dbOffer) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not get offer from DB for deletion'
                )
            );
        }

        // Call function for deletion
        return $this->removeOffer($vOffer, $dbOffer);
    }

    /**
     * @param Offer $offer
     * @param AffiliateNetwork $network
     * @return null|Offer
     */
    public function addOfferDataFromNetwork(Offer $offer, AffiliateNetwork $network)
    {
        // No offer no party
        if (empty($offer)) return null;

        // No network? return same offer
        if (empty($network)) return $offer;

        // Set fix properties
        $offer->setAffiliateNetworkId($network->getId());
        $offer->setInternalNetworkId($network->getInternalId());

        // Set conditional properties
        if ($offer->isOperatorBased() == null) {
            $offer->setOperatorBased($network->isOperatorBased());
        }
        if ($offer->isAuto() == null) {
            $offer->setAuto($network->isAuto());
        }
        if ($offer->isAutoPayout() == null) {
            $offer->setAutoPayout($network->isAutoPayout());
        }

        // Return repopulated offer
        return $offer;
    }

    /**
     * @param array $data
     * @return array
     */
    public function addNetworkOffers($data = array())
    {
        // If no network specified, return error
        if (empty($data['affiliate_network_id'])) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'No network specified for adding offers'
                )
            );
        }

        // Try to get network id
        $networkId = $data['affiliate_network_id'];

        // Try to get network from DB
        /** @var array $networks */
        $network = $this->networksRepo->get($networkId);

        // Check we got network, otherwise error
        if (!isset($network) || empty($network)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Network not integrated into system!'
                )
            );
        }

        // Parse db data to network
        $network = new AffiliateNetwork($network);

        // Unset that from filters
        unset($data['affiliate_network_id']);

        // Time to check if network has API or not
        if ($network->hasAPI()) {

            // Got mappings, get the network API offers
            /** @var array $afOffers */
            $afOffers = $this->getNetworkOffers($network->getNetworkCode(), $data);

            // Filter only non already integrated offers
            /** @var array $aOffers */
            $aOffers = array_filter($afOffers['offers'], function (Offer $entry) {
                return !$this->offersRepo->getByExternalId($entry->getExternalId());
            });

        // No API one
        } else {

            // Get all offers from internal
            /** @var array $afOffers */
            $afOffers = $this->internalApi->payloadToOffers($this->internalApi->listOffers($data));

            // Filter only non already integrated offers and only for the selected network
            /** @var array $aOffers */
            $aOffers = array_filter($afOffers['offers'], function (Offer $entry) use ($network) {
                return $network->getInternalId() == $entry->getInternalNetworkId() && !$this->offersRepo->getByInternalId($entry->getInternalId());
            });
        }

        // Filter if search provided
        if (isset($data['search']) && !empty($search = $data['search'])) {
            $aOffers = array_values(array_filter($aOffers, function (Offer $entry) use ($search) {
                return str_contains(strtolower($entry->getName() ?: $entry->getSourceName()), strtolower($search));
            }));
        }

        // Unset that from filters
        unset($data['search']);

        // Init feedback
        $result = '';

        // Init offers count
        $offersCount = 0;

        // Loop over non-integrated offers
        /** @var Offer $offer */
        foreach($aOffers as $offer) {

            // Merge network relevant data into offer
            $offer = $this->addOfferDataFromNetwork($offer, $network);

            // If no country set and only one available, set it
            if (empty($offer->getCountry()) && count($countries = $offer->getCountries()) == 1) {
                $offer->setCountry($countries[0]);
            }

            // Call offer addition function
            $res = $this->addOffer($offer);

            // Check if any error
            if (isset($res['error'])) {
                $result .= Util::multiLog($res['error']['message'], 'error');

            // Check for success feedback
            } else if (isset($res['success'])) {

                // Set output if any
                $result .= isset($res['output']) ? $res['output'] : "";

                // Increase offers counter
                $offersCount++;

            // Unknown error
            } else {
                $result .= Util::multiLog("Unknown error adding offer {$offer->getName()}", 'error');
            }
        }

        // If created any
        if ($offersCount > 0) {

            // Append to log the counter
            $result = Util::multiLog("Created ${offersCount} offers for current affiliate network", 'info', $result);
        }

        // Return success without message
        return array('success' => true, 'output' => $result);
    }

    /**
     * @return string
     */
    public function updateOffers()
    {
        // Get Internal API offers
        /** @var array $res */
        $res = $this->getInternalOffers();

        // Group offers by network
        /** @var array $aOffers */
        $aOffers = array_reduce($res['offers'], function($acc, Offer $entry) {

            /** @var string|integer $networkId */
            $networkId = $entry->getInternalNetworkId();

            // If not set, init it
            if (!isset($acc[$networkId])) {
                $acc[$networkId] = array();
            }

            // Add the offer to the group
            $acc[$networkId][] = $entry;

            // Return accumulate array
            return $acc;

        }, array());

        // Call aux function and return its output
        return $this->updateGroupedOffers($aOffers);
    }

    /**
     * @param string $network
     * @return string
     */
    public function updateNetworkOffers($network = '')
    {
        // If no network specified, return empty
        if (empty($network)) return '';

        // Get Internal API offers
        /** @var array $res */
        $res = $this->getInternalOffers();

        // Filter offers by network
        /** @var array $aOffers */
        $aOffers = array_filter($res['offers'], function(Offer $entry) use ($network) {
            return $entry->getInternalNetworkId() == $network;
        });

        // Call aux function and return its output
        return $this->updateGroupedOffers(array($network => $aOffers));
    }

    /**
     * @param array $aOffers
     * @return string
     */
    protected function updateGroupedOffers($aOffers = array())
    {
        // Holder for output echoing
        $output = "";

        // Counter for processed offers
        $processedOffersCount = 0;

        // Counter for deleted offers
        $deletedOffersCount = 0;

        // Counter for updated offers
        $updatedOffersCount = 0;

        // Holder for API offers
        $apiOffers = null;

        // Loop over Internal API offers groups by Affiliate Network
        /** @var array $offers */
        foreach($aOffers as $networkId => $offers) {

            // Try to get network for mappings
            /** @var array $networks */
            $networks = $this->networksRepo->query(array('internal_id' => $networkId));

            // Check we got network, otherwise skip
            if (!isset($networks) || empty($networks)) continue;

            // Parse network
            $network = new AffiliateNetwork($networks[0]);

            // Log it, but don't verbose it
            Util::multiLog("Getting offers from network {$network->getName()}", "info");

            // Time to check if network has API or not
            if ($network->hasAPI()) {

                // Got mappings, get the network API offers
                /** @var array $afOffers */
                $afOffers = $this->getNetworkOffers($network->getNetworkCode());

                // Get inner value
                $afOffers = $afOffers['offers'];

            // No API one
            } else {

                // Get all offers from internal
                /** @var array $afOffers */
                $afOffers = $apiOffers ?: $this->internalApi->payloadToOffers($this->internalApi->listOffers());

                // Filter only non already integrated offers and only for the selected network
                /** @var array $afOffers */
                $afOffers = array_filter($afOffers['offers'], function (Offer $entry) use ($network) {
                    return $network->getInternalId() == $entry->getInternalNetworkId();
                });
            }

            // Loop over Internal grouped offers
            /** @var Offer $vOffer */
            foreach($offers as $vOffer) {

                // Get offer mappings from DB for this Internal Offer
                /** @var Offer $dbOffer */
                $dbOffer = $this->dbPayloadToOffer($this->offersRepo->getByInternalId($vOffer->getInternalId()));

                // Check we got the mapped offer, otherwise skip
                if (empty($dbOffer)) continue;

                // Increment processed offers
                $processedOffersCount++;

                // Filter only already integrated offers
                /** @var Offer $afOffer */
                $afOffer = array_values(array_filter($afOffers, function(Offer $entry) use ($dbOffer, $network) {
                    return $network->hasAPI() ?
                        $entry->getExternalId() == $dbOffer->getExternalId() :
                        $entry->getInternalId() == $dbOffer->getInternalId();
                }));

                // If it does not exists, perform the required logic
                if (empty($afOffer)) {

                    // Logs
                    $output = Util::multiLog("Missing offer {$vOffer->getName()}", "info", $output);

                    // Call remove function
                    $result = $this->removeOffer($vOffer, $dbOffer);

                    // Update output log
                    $output .= isset($result['output']) ? $result['output'] : "";

                    // Check if it was success, so we update deleted offers count
                    if (isset($result['success'])) $deletedOffersCount++;

                // It exists, check payout and other relevant fields
                } else {

                    // Get object from array
                    $afOffer = $afOffer[0];

                    // Logs
                    Util::multiLog("Checking payout for offer {$vOffer->getName()}", "info");

                    // Check payout
                    if ($afOffer->getPayout() != $vOffer->getPayout() && $dbOffer->isAutoPayout()) {

                        // Not equal, set new one
                        $vOffer->setPayout($afOffer->getPayout());

                        // Update offer in internal API
                        $updatedOffer = $this->internalApi->updateOffer($this->internalApi->offerToPayload($vOffer));

                        // Check if no success
                        if (empty($updatedOffer)) {

                            // Log
                            $output = Util::multiLog("Could not update offer {$vOffer->getName()}", "error", $output);

                        // Success
                        } else {

                            // Log
                            $output = Util::multiLog("Updated payout for offer {$vOffer->getName()}", "info", $output);

                            // Increment counter
                            $updatedOffersCount++;
                        }
                    }

                    // Update/Create campaigns. Check if Auto, otherwise skip
                    if ($dbOffer->isAuto()) {

                        // Merge everything
                        $offer = $this->mergeDbOfferWithOffer($dbOffer, $this->mergeInternalOfferWithOffer($vOffer, $afOffer));

                        // TODO: Comment/Uncomment below ensuring offers are properly updated
                        $output .= $this->campaignsService->updateOfferCampaigns($offer);

                        // TODO: Uncomment this after ensuring offers are properly created
                        $output .= $this->campaignsService->createOfferCampaigns($offer, true);
                    }
                }
            }
        }

        // Check if we updated any
        if ($processedOffersCount > 0) {
            $output = Util::multiLog("Processed ${processedOffersCount} offers", 'info', $output);
        }

        // Check if we updated any
        if ($updatedOffersCount > 0) {
            $output = Util::multiLog("Updated ${updatedOffersCount} offers", 'info', $output);
        }

        // Check if we updated any
        if ($deletedOffersCount > 0) {
            $output = Util::multiLog("Deleted ${deletedOffersCount} offers", 'info', $output);
        }

        // Return output
        return $output;
    }

    /**
     * @param Offer $vOffer
     * @param Offer $dbOffer
     * @return array
     */
    protected function removeOffer($vOffer, $dbOffer)
    {
        // Init feedback
        $output = '';

        // First merge both offers
        $offer = $this->mergeDbOfferWithOffer($dbOffer, $vOffer);

        // Update campaigns before removal. Check if Auto, otherwise skip
        if ($dbOffer->isAuto()) {

            // TODO: Comment/Uncomment below ensuring offers are properly updated
            $output .= $this->campaignsService->updateOfferCampaigns($offer, true);
        }

        // Remove offer from internal API
        $deletedOffer = $this->internalApi->deleteOffer($vOffer->getInternalId());

        // Logs
        Util::multiLog("Removing offer {$vOffer->getName()}", "info", $output);

        // Check if no success
        if (empty($deletedOffer)) {

            // Logs
            $output = Util::multiLog("Could not delete offer {$vOffer->getName()} from API", "error", $output);

            // Return result
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => "Could not delete offer {$vOffer->getName()} from API",
                ),
                'output' => $output
            );
        }

        // Now delete from DB
        $deletedOffer = $this->offersRepo->delete($dbOffer->getId());

        // Check if error
        if (!$deletedOffer) {

            // Logs
            $output = Util::multiLog("Could not delete offer {$dbOffer->getId()} ({$vOffer->getName()}) from DB", "error", $output);

            // Return result
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => "Could not delete offer {$dbOffer->getId()} ({$vOffer->getName()}) from DB",
                ),
                'output' => $output
            );
        }

        // Init feedback
        $output .= Util::multiLog("Removed offer {$vOffer->getName()}", 'info');

        // Return result
        return array(
            'success' => true,
            'output' => $output
        );
    }
}