<?php

namespace VoluumAffiliate\Services;

use VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Networks\IInternalNetworkApi;

/**
 * Class AffiliateNetworksService
 * @package VoluumAffiliate\Services
 */
class AffiliateNetworksService {

    /** @var IAffiliateNetworksRepository $networksRepo */
    protected $networksRepo;

    /** @var IInternalNetworkApi $internalApi */
    protected $internalApi;

    /**
     * @param IAffiliateNetworksRepository $networksRepo
     * @param IInternalNetworkApi $internalApi
     */
    public function __construct(IAffiliateNetworksRepository $networksRepo, IInternalNetworkApi $internalApi)
    {
        $this->networksRepo = $networksRepo;
        $this->internalApi = $internalApi;
    }

    /**
     * @param array $data
     * @return array
     */
    public function find($data = array())
    {
        return $this->networksRepo->query($data);
    }

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->networksRepo->get($id);
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function getByCode($code)
    {
        /** @var array $networksConfig */
        $networksConfig = config('voluum-affiliate.networks');

        // No configured network
        if (!isset($networksConfig[$code])) return null;

        // Get network from DB
        $networks = $this->find(array('network_code' => $code));

        // Return it or null
        return empty($networks) ? null : $networks[0];
    }

    /**
     * @param $data
     * @return array
     */
    public function dbPayloadToAffiliateNetworks($data)
    {
        // Init collection
        $parsed = array();

        // Loop and create objects
        foreach($data as $entry) {
            $parsed[] = new AffiliateNetwork($entry);
        }

        // Return parsed collection
        return $parsed;
    }

    /**
     * @param AffiliateNetwork $dbNetwork
     * @param AffiliateNetwork $network
     * @return mixed
     */
    public function mergeDbNetworkWithNetwork($dbNetwork, $network)
    {
        // No offer, return just internal
        if (empty($network)) return $dbNetwork;

        // Check if got internal
        if (!empty($dbNetwork)) {

            /** ALWAYS SET FROM DB PROPERTIES */
            if ($id = $dbNetwork->getId()) {
                $network->setId($id);
            }

            if ($network_code = $dbNetwork->getNetworkCode()) {
                $network->setNetworkCode($network_code);
            }

            if ($operator_based = $dbNetwork->isOperatorBased()) {
                $network->setOperatorBased($operator_based);
            }

            if ($auto = $dbNetwork->isAuto()) {
                $network->setAuto($auto);
            }

            if ($auto_payout = $dbNetwork->isAutoPayout()) {
                $network->setAutoPayout($auto_payout);
            }
        }

        // Return the network
        return $network;
    }

    /**
     * @param array $data
     * @return array
     */
    public function allFromApi($data = array())
    {
        return $this->internalApi->payloadToAffiliateNetworks($this->internalApi->listAffiliateNetworks($data));
    }

    /**
     * @return mixed
     */
    public function getImplemented() {
        return config('voluum-affiliate.networks');
    }

    /**
     * @param array $data
     * @return null|AffiliateNetwork
     */
    public function formToAffiliateNetwork($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Get proper data from input
        $parsedData = array(
            'internal_id' => $data['internal_id'],
            'network_code' => $data['network_code'],
            'name' => $data['name']
        );

        // If set, assign DB id
        if (isset($data['id'])) {
            $parsedData['id'] = $data['id'];
        }

        // Check operator
        if (isset($data['operator_based'])) {
            $parsedData['operator_based'] = $data['operator_based'];
        }

        // Check auto
        if (isset($data['auto'])) {
            $parsedData['auto'] = $data['auto'];
        }

        // Check auto-payout
        if (isset($data['auto_payout'])) {
            $parsedData['auto_payout'] = $data['auto_payout'];
        }

        // Create Affiliate Network object with fixed data
        return new AffiliateNetwork($parsedData);
    }

    /**
     * @param AffiliateNetwork $afn
     * @return array|null
     */
    public function affiliateNetworkToDbPayload($afn)
    {
        // Check
        if (!isset($afn) || empty($afn)) return null;

        // Create offer mappings for DB
        return array(
            'id' => $afn->getId(),
            'internal_id' => $afn->getInternalId(),
            'network_code' => $afn->getNetworkCode(),
            'operator_based' => $afn->isOperatorBased(),
            'auto' => $afn->isAuto(),
            'auto_payout' => $afn->isAutoPayout()
        );
    }

    /**
     * @param array $data
     * @return array
     */
    public function add($data = array()) {

        // No data, no process
        if (empty($data)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for adding network'
                )
            );
        }

        /** @var AffiliateNetwork $network */
        $network = $this->formToAffiliateNetwork($data);

        // Add to DB
        $result = $this->networksRepo->create($this->affiliateNetworkToDbPayload($network));

        // Error adding to DB
        if (empty($result) || !$result) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not add network to DB'
                )
            );
        }

        // Return success
        return array('success' => true);
    }

    /**
     * @param array $data
     * @return array
     */
    public function update($data = array()) {

        // No data, no process
        if (empty($data)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for updating network'
                )
            );
        }

        /** @var AffiliateNetwork $network */
        $network = $this->formToAffiliateNetwork($data);

        // TODO: Update into API: Right now we don't do it, but can do it with an internal API adaptor

        // Update into DB
        $result = $this->networksRepo->update($network->getId(), $this->affiliateNetworkToDbPayload($network));

        // Error adding to DB
        if (empty($result) && $result != 0) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not update network into DB'
                )
            );
        }

        // Return success
        return array('success' => true);
    }

    /**
     * @param array $fromApi
     * @param array $fromDB
     * @return mixed
     */
    public function mergeCollections($fromApi, $fromDB) {

        // Loop over all API data
        /** @var AffiliateNetwork $entry */
        foreach($fromApi as $entry) {

            // Try to filter DB ones
            $filtered = array_values(array_filter($fromDB, function(AffiliateNetwork $item) use ($entry) {
                return $entry->getInternalId() == $item->getInternalId();
            }));

            // Check if got something
            if (!empty($filtered)) {

                /** @var AffiliateNetwork $fEntry */
                $fEntry = $filtered[0];

                // Merge
                $entry = $this->mergeDbNetworkWithNetwork($fEntry, $entry);
            }
        }

        // Return merged stuff
        return $fromApi;
    }

    /**
     * @param $data
     * @return array
     */
    public function filterImplemented($data) {

        // Get implemented ones
        $implemented = array_keys($this->getImplemented());

        // Try to filter by implemented code
        $filtered = array_values(array_filter($data, function(AffiliateNetwork $entry) use ($implemented) {
            return in_array($entry->getNetworkCode(), $implemented);
        }));

        // Return filtered stuff
        return $filtered;
    }

    /**
     * @param $data
     * @return array
     */
    public function filterIntegrated($data) {

        // Get integrated ones
        $integrated = $this->dbPayloadToAffiliateNetworks($this->find());

        // Try to filter by integrated internal id
        $filtered = array_values(array_filter($data, function(AffiliateNetwork $entry) use ($integrated) {
            $filtered2 = array_values(array_filter($integrated, function(AffiliateNetwork $entry2) use ($entry) {
                return $entry2->getInternalId() == $entry->getInternalId();
            }));
            return !empty($filtered2);
        }));

        // Return filtered stuff
        return $filtered;
    }

    /**
     * @return mixed|array
     */
    public function allIntegrated()
    {
        // Get available affiliate networks from DB (integrated ones)
        $iNetworks = $this->dbPayloadToAffiliateNetworks($this->find());

        // Try to get networks also from API
        $apiNetworks = $this->allFromApi();

        // Try to filter by integrated internal id
        $apiNetworks = array_values(array_filter($apiNetworks, function(AffiliateNetwork $entry) use ($iNetworks) {
            $filtered2 = array_values(array_filter($iNetworks, function(AffiliateNetwork $entry2) use ($entry) {
                return $entry2->getInternalId() == $entry->getInternalId();
            }));
            return !empty($filtered2);
        }));

        // Merge them
        $networks = $this->mergeCollections($apiNetworks, $iNetworks);

        // Return them
        return $networks;
    }

    /**
     * @param string $id
     * @return AffiliateNetwork
     */
    public function getIntegratedById($id = '')
    {
        // Get available affiliate networks from DB (integrated ones)
        $dbNetwork = new AffiliateNetwork($this->get($id));

        // Try to get networks also from API
        $internalNetwork = $this->internalApi->payloadToAffiliateNetwork($this->internalApi->getAffiliateNetwork($dbNetwork->getInternalId()));

        // Merge them
        $network = $this->mergeDbNetworkWithNetwork($dbNetwork, $internalNetwork);

        // Return them
        return $network;
    }

}