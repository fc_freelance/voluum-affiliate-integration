<?php

namespace VoluumAffiliate\Services;

use stdClass;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Models\TrafficSource;
use VoluumAffiliate\Networks\IInternalNetworkApi;
use VoluumAffiliate\utils\Countries;
use VoluumAffiliate\Utils\Util;

/**
 * Class CampaignsService
 * @package VoluumAffiliate\Services
 */
class CampaignsService {

    /** @var IInternalNetworkApi $internalApi */
    protected $internalApi;

    /** @var TrafficSourcesService $sourcesService */
    protected $sourcesService;

    /** @var array $config */
    protected $config = array();

    /**
     * @param IInternalNetworkApi $internalApi
     * @param TrafficSourcesService $sourcesService
     */
    public function __construct(IInternalNetworkApi $internalApi, TrafficSourcesService $sourcesService)
    {
        $this->internalApi = $internalApi;
        $this->sourcesService = $sourcesService;
        $this->config = config('voluum-affiliate');
    }

    /**
     * @return array
     */
    public function allFromApi()
    {
        // Get data from API
        $results = $this->internalApi->listCampaigns();

        // Return them
        return $results;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function allFromReportingApi($data = array())
    {
        // Force we report by campaign
        $data['group_by'] = 'campaign';

        // Get data from API
        $results = $this->internalApi->getReport($data);

        // Return them
        return $results;
    }

    /**
     * @param array|stdClass $data
     * @return array
     */
    // FIXME: Remove this if adopting the regular API rather than report one is OK (so currently we don't use this)
    public function reportToPayload($data = array())
    {
        return $this->internalApi->campaignsReportToPayload($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportCampaignsToPayload($data = array())
    {
        return $this->internalApi->reportCampaignsToPayload($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportCampaignToPayload($data = array())
    {
        return $this->internalApi->reportCampaignToPayload($data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function liveReportByCampaign($data = array())
    {
        return $this->internalApi->getLiveReport($data);
    }

    /**
     * @param TrafficSource $source
     * @param Offer $offer
     * @return bool
     */
    public function isTrafficSourceEligibleForCampaign(TrafficSource $source, Offer $offer)
    {
        // Start flag as true
        $b = true;

        // Source should be in 'auto'
        $b = $b && $source->isAuto();

        // Offer should be in auto too
        $b = $b && $offer->isAuto();

        // If source is incent, offer should be too
        $b = $b && (!$source->isIncent() || $offer->isIncent());

        // If offer is operator based, skip
        $b = $b && !$offer->isOperatorBased();

        // Check offer country is supported on target source
        $b = $b && (empty($offer->getCountry()) || empty($source->getCountries()) || in_array($offer->getCountry(), $source->getCountries()));

        // Check offer platform(s) are supported on target source
        $b = $b && !empty(array_intersect($offer->getPlatforms(), $source->getPlatforms()));

        // Check cost model is the same
        $b = $b && ($source->getCostModel() == $offer->getCostModel() ||
            in_array($offer->getCostModel(), array('CPA', 'CPI')) && in_array($source->getCostModel(), array('CPA', 'CPI')));

        // Check offer daily caps are equal or above the min daily caps in source
        $b = $b && $offer->getCaps() >= $source->getMinCaps();

        // Return processed flag result
        return $b;
    }

    /**
     * @param Offer $offer
     * @param bool $fromUpdate
     * @return string
     */
    public function createOfferCampaigns($offer, $fromUpdate = false)
    {
        // Check if Offer
        if (empty($offer)) return Util::multiLog("Missing offer data for creating campaigns", 'error');

        /** @var string $result */
        $result = "";

        // Check if offer is not auto, then skip
        if (!$offer->isAuto()) return $result;

        /** @var integer $campaignsCount */
        $campaignsCount = 0;

        /** @var integer $recoveredCampaignsCount */
        $recoveredCampaignsCount = 0;

        /** @var array $tSources */
        $tSources = $this->sourcesService->findFromApi();

        // Get internal campaigns, but from reporting API!!
        $rawInternalCampaigns = $this->allFromReportingApi();

        // Parse them
        $internalCampaigns = $this->reportCampaignsToPayload($rawInternalCampaigns->rows);

        /** @var TrafficSource $tSource */
        foreach ($tSources as $tSource) {

            // Get source from DB for mappings
            $dbSource = $this->sourcesService->find(array('internal_id' => $tSource->getInternalId()));

            // If no db source, skip
            if (empty($dbSource)) continue;

            // Set object
            $dbSource = $this->sourcesService->dbPayloadToTrafficSource($dbSource[0]);

            // And merge both
            $tSource =  $this->sourcesService->mergeDbSourceWithSource($dbSource, $tSource);

            // Check that the tuple TrafficSource - Offer is entitled for a Campaign, otherwise skip to next
            if (!$this->isTrafficSourceEligibleForCampaign($tSource, $offer)) continue;

            // Check against names
            $filteredCampaigns = array_values(array_filter($internalCampaigns, function($entry) use ($offer, $tSource) {
                return $tSource->getName() == $entry['trafficSourceName'] &&
                    $entry['country'] == Countries::getNameByCode($offer->getCountry()) &&
                    $entry['name'] == $offer->getComposedName();
            }));

            // Init error message for compatibility
            $campaignCreationError = '';

            // Check if no one
            if (empty($filteredCampaigns)) {

                // Try to create campaign in Internal API
                $campaign = $this->internalApi->createCampaignFromOffer($offer, $tSource, $this->config['defaults']['offer_to_campaign_payout_multiplier']);

                // General error message
                $campaignCreationError = "Could not create campaign for traffic source {$tSource->getName()} with offer {$offer->getName()}";

            // It already exists, and also active
            } else if ($filteredCampaigns[0]['active']) {

                // Just log this message
                $campaignCreationError = "Attempt to create an already existing campaign for traffic source {$tSource->getName()} with offer {$offer->getName()}";

            // It's archived!
            } else {

                // Try to recover it
                $recoveredCampaign = $this->internalApi->recoverCampaign($filteredCampaigns[0]['id']);

                // Check if not properly recovered
                if (empty($recoveredCampaign) || $recoveredCampaign->status != 204 || (isset($recoveredCampaign->data) && isset($recoveredCampaign->data->error))) {

                    // Just log this message
                    $campaignCreationError = "Failed to recover an already existing but archived campaign for traffic source {$tSource->getName()} with offer {$offer->getName()}";

                // Success recovering
                } else {

                    // Form data to update
                    $updatedCampaignData = $this->internalApi->generateCampaignDataFromOffer($offer, $tSource, $this->config['defaults']['offer_to_campaign_payout_multiplier']);

                    // Set the id
                    $updatedCampaignData['id'] = $filteredCampaigns[0]['id'];

                    // Try to update it
                    $campaign = $this->internalApi->updateCampaign($updatedCampaignData);

                    // Check if not properly updated
                    if (empty($campaign) || isset($campaign->data->error)) {

                        // Just log this message
                        $campaignCreationError = "Failed to update a recovered campaign for traffic source {$tSource->getName()} with offer {$offer->getName()}";

                    // This is success, increment recovered
                    } else {
                        $recoveredCampaignsCount++;
                    }
                }
            }

            // Check if no response at all, and build message if any
            if (!isset($campaign) || empty($campaign)) {
                $result = Util::multiLog($campaignCreationError, 'error', $result);

            // Check if error in request and pass it to result
            } else if (isset($campaign->data->error)) {

                // Only if not in update mode
//                if (!$fromUpdate) {
                    $result = Util::multiLog("${campaignCreationError} : {$campaign->data->error->description}", 'error', $result);
//                }

            // Everything ok, increment counter only if created ....
            } else if (!isset($recoveredCampaign)) {
                $campaignsCount++;
            }
        }

        // Check if we created any
        if ($campaignsCount > 0) {
            $result = Util::multiLog("Created ${campaignsCount} campaigns for offer {$offer->getName()}", 'info', $result);
        }

        // Check if we created any
        if ($recoveredCampaignsCount > 0) {
            $result = Util::multiLog("Recovered ${recoveredCampaignsCount} campaigns for offer {$offer->getName()}", 'info', $result);
        }

        // Return final result
        return $result;
    }


    /**
     * @param Offer $offer
     * @param bool $remove
     * @return string
     */
    public function updateOfferCampaigns($offer, $remove = false)
    {
        // Check if Offer
        if (empty($offer)) return Util::multiLog("Missing offer data for updating campaigns", 'error');

        /** @var string $result */
        $result = "";

        // Check if offer is not auto, then skip
        if (!$offer->isAuto()) return $result;

        /** @var integer $removedCampaignsCount */
        $removedCampaignsCount = 0;

        /** @var integer $updatedCampaignsCount */
        $updatedCampaignsCount = 0;

        // Get internal campaigns, they can change from the last time
        $internalCampaigns = $this->allFromApi();

        // Loop over campaigns
        foreach ($internalCampaigns as $iCampaign) {

            // Get traffic source for campaign
            $source = $this->sourcesService->find(array('internal_id' => $iCampaign->trafficSource->id));

            // If source is empty, just skip, we don't want to automate its campaign
            if (empty($source)) continue;

            // Got it, parse it
            $source = $this->sourcesService->dbPayloadToTrafficSource($source[0]);

            // If no eligible, skip too
            // TODO: Look here, maybe it's too much constraint
            if (!$this->isTrafficSourceEligibleForCampaign($source, $offer)) continue;

            // Flag to state if relevant to process campaign
            $campaignContainsOffer = false;

            // Anf flag to state if update it at all
            $needsUpdate = $remove;

            // Loop over campaign paths
            foreach ($iCampaign->paths as $pathKey => $path) {

                // Get matched offer in path
                $matchedOffers = array_filter($path->offers, function($entry) use ($offer) {
                    return $entry->offer->id == $offer->getInternalId();
                });

                // Check if matched offer in path, otherwise continue
                if (empty($matchedOffers)) {

                    // It's removal, continue
                    if ($remove) continue;

                    // But if not removing and other offers in any path, just break to outer
                    if (!empty($path->offers)) break;
                }

                // The path contains the matched offer
                // Look if it contains more offers
                if (count($path->offers) > count($matchedOffers)) {

                    // If it's not removal, break to next campaign
                    if (!$remove) break;

                    // Then just remove the offer from it
                    foreach ($matchedOffers as $key => $value) {
                        unset($path->offers[$key]);
                    }

                    // Normalize it
                    $path->offers = array_values($path->offers);

                    // Logs
                    $result = Util::multiLog("Removing offer {$offer->getName()} from path in {$iCampaign->name}", "info", $result);

                // Path only contains this offer, remove it (if removal mode)
                } else if ($remove) {

                    // Remove it
                    unset($iCampaign->paths[$pathKey]);

                    // Logs
                    $result = Util::multiLog("Removing path for offer {$offer->getName()} from campaign {$iCampaign->name}", "info", $result);
                }

                // Campaign contains offer, so set flag
                $campaignContainsOffer = true;
            }

            // If campaign didn't contain the offer, skip to next
            if (!$campaignContainsOffer) continue;

            // If removal mode
            if ($remove) {

                // Get remaining active paths for campaign
                $cActivePaths = array_filter($iCampaign->paths, function ($entry) {
                    return $entry->active;
                });

                // Check if no active paths remaining
                if (empty($cActivePaths)) {

                    // Then remove the campaign
                    $deletedCampaign = $this->internalApi->deleteCampaign($iCampaign->id);

                    // Check if no success
                    if (empty($deletedCampaign)) {

                        // Logs
                        $result = Util::multiLog("Could not delete campaign {$iCampaign->name} with offer {$offer->getName()} from API", "error", $result);

                    // Success
                    } else {

                        // Logs
                        $result = Util::multiLog("Removed campaign {$iCampaign->name} with offer {$offer->getName()}", "info", $result);

                        // Increment
                        $removedCampaignsCount++;
                    }

                    // And continue
                    continue;
                }

                // No empty campaign log that we're going to update paths only
                $result = Util::multiLog("Updating paths for campaign {$iCampaign->name} with offer {$offer->getName()}", 'info', $result);

                // Set update flag
                $needsUpdate = true;

            // Not removal, update mode
            } else {

                // Update payout if set to auto in both sides
                if ($offer->isAutoPayout() && $source->isAutoPayout()) {

                    // Update campaign payout
                    switch ($iCampaign->costModel) {

                        // CPA (CPI)
                        case 'CPA':

                            // expected multiplied offer value
                            $expectedPayout = $this->config['defaults']['offer_to_campaign_payout_multiplier'] * $offer->getPayout();

                            // Check if different values
                            if (abs($expectedPayout - $iCampaign->cpa) > 0.000001) {

                                // Log
                                $result = Util::multiLog("Updating campaign payout from {$iCampaign->cpa} to ${expectedPayout} for campaign {$iCampaign->name} with offer {$offer->getName()}", 'info', $result);

                                // Update campaign payout
                                $iCampaign->cpa = $expectedPayout;

                                // Set update flag
                                $needsUpdate = true;
                            }
                            break;

                        // CPC
                        case 'CPC':
                            // TODO: Apply proper logic here
                            break;
                    }
                }
            }

            // If no need for update, just skip
            if (!$needsUpdate) continue;

            // Paths remaining, let's update the campaign
            $updatedCampaign = $this->internalApi->updateCampaign($iCampaign);

            // Check if no success
            if (empty($updatedCampaign)) {

                // Logs
                $result = Util::multiLog("Could not update campaign {$iCampaign->name} with offer {$offer->getName()} in API", "error", $result);

            // Success
            } else {

                // Logs
                $result = Util::multiLog("Updated campaign {$iCampaign->name} with offer {$offer->getName()}", "info", $result);

                // Increment counter
                $updatedCampaignsCount++;
            }
        }

        // Check if we updated any
        if ($updatedCampaignsCount > 0) {
            $result = Util::multiLog("Updated ${updatedCampaignsCount} campaigns for offer {$offer->getName()}", 'info', $result);
        }

        // Check if we deleted any
        if ($removedCampaignsCount > 0) {
            $result = Util::multiLog("Deleted ${removedCampaignsCount} campaigns for offer {$offer->getName()}", 'info', $result);
        }

        // Return final result
        return $result;
    }

    /**
     * @param array $campaigns
     * @param TrafficSource $tSource
     * @param array $qs
     * @return array
     */
    public function filterPublisherCampaigns($campaigns, $tSource, $qs = array())
    {
        // Filter campaigns for the required source, with only one path and with one offer in that path
        $campaigns = array_values(array_filter($campaigns, function($entry) use ($tSource) {

            // Boolean flag, check traffic source
            $b = $entry->trafficSource->id == $tSource->getInternalId();

            // Only proceed if met
            if ($b) {

                // Get reference to paths
                $cPaths = $entry->paths;

                // Check there's ony ONE path
                $b = $b && !empty($cPaths) && count($cPaths) == 1;

                // Only proceed if met
                if ($b) {

                    // Get reference to path offers
                    $pOffers = $cPaths[0]->offers;

                    // Check that only ONE offer is set in the ONLY path
                    $b = $b && !empty($pOffers) && count($pOffers) == 1;
                }
            }

            return $b;
        }));

        // Now apply limit if got it
        if (isset($qs['limit']) || isset($qs['offset'])) {
            $campaigns = array_slice($campaigns, isset($qs['offset']) ? intval($qs['offset']) : 0, isset($qs['limit']) ? intval($qs['limit']) : null);
        }

        // Return collection
        return $campaigns;
    }

    /**
     * @param array $campaigns
     * @param array $offers
     * @return array
     */
    public function generatePublisherCampaignsPayload($campaigns, $offers)
    {
        // Now we need to map the values on each one
        $campaigns = array_map(function($entry) use ($offers) {

            // Init payload
            $payload = array(
                'id' => $entry->id,
                'name' => $entry->namePostfix,
                'country' => (isset($entry->country) && !empty($entry->country)) ? $entry->country->code : null,
                'url' => $entry->url,
                'impressionUrl' => $entry->impressionUrl,
                'costModel' => $entry->costModel,
                'payout' => ($entry->costModel == 'CPA') ?  $entry->cpa : ($entry->costModel == 'CPC' ? $entry->cpc : null),
                'created' => $entry->createdTime,
                'updated' => $entry->updatedTime,
            );

            // Get underlying offer
            $offerId = $entry->paths[0]->offers[0]->offer->id;

            // Get proper offer from offers
            $offer = array_values(array_filter($offers, function(Offer $oEntry) use ($offerId) {
                return $oEntry->getInternalId() == $offerId;
            }));

            // Check if not offer, then just return campaign data??
            // TODO: Or simply don't return it??
            if (!empty($offer)) {

                /** @var Offer $offer */
                $offer = $offer[0];

                // Adjust payload
                $payload['previewUrl'] = $offer->getPreviewUrl();
                $payload['previewImage'] = $offer->getPreviewImage();
                $payload['platforms'] = $offer->getPlatforms();
                $payload['dailyCaps'] = $offer->getCaps();
                $payload['creativesUrl'] = $offer->getCreativesUrl();
                $payload['incent'] = !!$offer->isIncent();
            }

            return $payload;

        }, $campaigns);

        return $campaigns;
    }
}