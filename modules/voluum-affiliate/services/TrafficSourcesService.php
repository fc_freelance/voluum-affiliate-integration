<?php

namespace VoluumAffiliate\Services;

use VoluumAffiliate\Database\Repositories\ITrafficSourcesRepository;
use VoluumAffiliate\Models\TrafficSource;
use VoluumAffiliate\Networks\IInternalNetworkApi;

/**
 * Class TrafficSourcesService
 * @package VoluumAffiliate\Services
 */
class TrafficSourcesService {

    /** @var ITrafficSourcesRepository $sourcesRepo */
    protected $sourcesRepo;

    /** @var IInternalNetworkApi $internalApi */
    protected $internalApi;

    /**
     * @param ITrafficSourcesRepository $sourcesRepo
     * @param IInternalNetworkApi $internalApi
     */
    public function __construct(ITrafficSourcesRepository $sourcesRepo, IInternalNetworkApi $internalApi)
    {
        $this->sourcesRepo = $sourcesRepo;
        $this->internalApi = $internalApi;
    }

    /**
     * @param array $data
     * @return array
     */
    public function find($data = array())
    {
        return $this->sourcesRepo->query($data);
    }

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->sourcesRepo->get($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function payloadToTrafficSources($data) {

        $parsed = array();

        foreach($data as $entry) {
            $parsed[] = new TrafficSource($entry);
        }

        return $parsed;
    }

    /**
     * @param $data
     * @return array
     */
    public function dbPayloadToTrafficSources($data) {

        $parsed = array();

        foreach($data as $entry) {
            $parsed[] = $this->dbPayloadToTrafficSource($entry);
        }

        return $parsed;
    }

    /**
     * @param $data
     * @return null|TrafficSource
     */
    public function dbPayloadToTrafficSource($data)
    {
        // Check
        if (!isset($data) || empty($data)) return null;

        // Parse countries
        if (!empty($countries = $data->countries)) {
            $countries = explode(',', $countries);
        }

        // Parse platforms
        if (!empty($platforms = $data->platforms)) {
            $platforms = explode(',', $platforms);
        }

        // Return parsed collection
        return new TrafficSource(array(
            'id' => $data->id,
            'internal_id' => $data->internal_id,
            'countries' => $countries,
            'platforms' => $platforms,
            'min_caps' => $data->min_caps,
            'cost_model' => $data->cost_model,
            'incent' => $data->incent,
            'auto' => $data->auto,
            'auto_payout' => $data->auto_payout
        ));
    }

    /**
     * @param TrafficSource $dbSource
     * @param TrafficSource $source
     * @return mixed
     */
    public function mergeDbSourceWithSource($dbSource, $source)
    {
        // No offer, return just internal
        if (empty($source)) return $dbSource;

        // Check if got internal
        if (!empty($dbSource)) {

            /** ALWAYS SET FROM DB PROPERTIES */
            if ($id = $dbSource->getId()) {
                $source->setId($id);
            }

            if ($platforms = $dbSource->getPlatforms()) {
                $source->setPlatforms($platforms);
            }

            if ($countries = $dbSource->getCountries()) {
                $source->setCountries($countries);
            }

            if ($cost_model = $dbSource->getCostModel()) {
                $source->setCostModel($cost_model);
            }

            if ($min_caps = $dbSource->getMinCaps()) {
                $source->setMinCaps($min_caps);
            }

            if ($incent = $dbSource->isIncent()) {
                $source->setIncent($incent);
            }

            if ($auto = $dbSource->isAuto()) {
                $source->setAuto($auto);
            }

            if ($auto_payout = $dbSource->isAutoPayout()) {
                $source->setAutoPayout($auto_payout);
            }
        }

        // Return the source
        return $source;
    }

    /**
     * @param array $data
     * @return array
     */
    public function findFromApi($data = array())
    {
        return $this->internalApi->payloadToTrafficSources($this->internalApi->listTrafficSources($data));
    }

    /**
     * @param array $id
     * @return array
     */
    public function getFromApi($id)
    {
        return $this->internalApi->payloadToTrafficSource($this->internalApi->getTrafficSource($id));
    }

    /**
     * @param array $data
     * @return null|TrafficSource
     */
    public function formToTrafficSource($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Get proper data from input
        $parsedData = array(
            'internal_id' => $data['internal_id'],
            'name' => $data['name']
        );

        // If set, assign DB id
        if (isset($data['id'])) {
            $parsedData['id'] = $data['id'];
        }

        // Check platforms
        if (isset($data['platforms']) && !empty($data['platforms'])) {
            $parsedData['platforms'] = $data['platforms'];
        }

        // Check available countries
        if (isset($data['countries']) && !empty($data['countries'])) {
            $parsedData['countries'] = $data['countries'];
        }

        // Check postback url
        if (isset($data['postback_url'])) {
            $parsedData['postback_url'] = $data['postback_url'];
        }

        // Check min caps
        if (isset($data['min_caps'])) {
            $parsedData['min_caps'] = $data['min_caps'];
        }

        // Check cost model
        if (isset($data['cost_model'])) {
            $parsedData['cost_model'] = $data['cost_model'];
        }

        // Check incent
        if (isset($data['incent'])) {
            $parsedData['incent'] = $data['incent'];
        }

        // Check auto
        if (isset($data['auto'])) {
            $parsedData['auto'] = $data['auto'];
        }

        // Check auto-payout
        if (isset($data['auto_payout'])) {
            $parsedData['auto_payout'] = $data['auto_payout'];
        }

        // Create Offer object with fixed data
        return new TrafficSource($parsedData);
    }

    /**
     * @param TrafficSource $afn
     * @return array|null
     */
    public function trafficSourceToDbPayload($afn)
    {
        // Check
        if (!isset($afn) || empty($afn)) return null;

        // Parse countries
        if (!empty($countries = $afn->getCountries())) {
            $countries = join(',', $countries);
        }

        // Parse platforms
        if (!empty($platforms = $afn->getPlatforms())) {
            $platforms = join(',', $platforms);
        }

        // Create traffic source mappings for DB
        return array(
            'id' => $afn->getId(),
            'internal_id' => $afn->getInternalId(),
            'countries' => $countries,
            'platforms' => $platforms,
            'min_caps' => $afn->getMinCaps() ?: null,
            'cost_model' => $afn->getCostModel(),
            'incent' => $afn->isIncent(),
            'auto' => $afn->isAuto(),
            'auto_payout' => $afn->isAutoPayout()
        );
    }

    /**
     * @param array $data
     * @return array
     */
    public function add($data = array()) {

        // No data, no process
        if (empty($data)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for adding traffic source'
                )
            );
        }

        /** @var TrafficSource $source */
        $source = $this->formToTrafficSource($data);

        // TODO: Add into internal API? Right now we don't, but could with just an adapter into internal API

        // Add to DB
        $result = $this->sourcesRepo->create($this->trafficSourceToDbPayload($source));

        // Error adding to DB
        if (empty($result) || !$result) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not add traffic source to DB'
                )
            );
        }

        // Return success
        return array('success' => true);
    }

    /**
     * @param array $data
     * @return array
     */
    public function update($data = array()) {

        // No data, no process
        if (empty($data)) {
            return array(
                'error' => array(
                    'code' => 400,
                    'message' => 'Missing data for updating traffic source'
                )
            );
        }

        /** @var TrafficSource $source */
        $source = $this->formToTrafficSource($data);

        // TODO: Update into internal API? Right now we don't, but could with just an adapter into internal API

        // Update into DB
        $result = $this->sourcesRepo->update($source->getId(), $this->trafficSourceToDbPayload($source));

        // Error adding to DB
        if (empty($result) || !$result) {
            return array(
                'error' => array(
                    'code' => 500,
                    'message' => 'Could not update traffic source into DB'
                )
            );
        }

        // Return success
        return array('success' => true);
    }
}