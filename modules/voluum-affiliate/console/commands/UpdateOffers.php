<?php

namespace VoluumAffiliate\Console\Commands;

use Illuminate\Console\Command;

use VoluumAffiliate\Jobs\UpdateOffers as UpdateOffersJob;

/**
 * Class UpdateOffers
 * @package VoluumAffiliate\Console\Commands
 */
class UpdateOffers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-offers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates offers.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UpdateOffersJob $job
     * @return mixed
     */
    public function handle(UpdateOffersJob $job)
    {
        // TODO: Enqueue it rather ...
        $job->handle();
    }
}
