<?php

namespace VoluumAffiliate\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\View\View;
use VoluumAffiliate\Console\Commands\UpdateOffers;
use VoluumAffiliate\Services\AffiliateNetworksService;
use VoluumAffiliate\Services\CampaignsService;
use VoluumAffiliate\Services\OffersService;
use VoluumAffiliate\Services\TrafficSourcesService;

/**
 * Class VoluumAffiliateServiceProvider
 * @package VoluumAffiliate\Providers
 */
class VoluumAffiliateServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
//    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'voluum-affiliate');

        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/../http/routes.php';
        }

        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('voluum-affiliate.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations'),
        ], 'migrations');

//        $this->publishes([
//            __DIR__.'/../database/seeds' => database_path('seeds'),
//        ], 'seeds');

//        $this->publishes([
//            __DIR__.'/../resources/views' => base_path('resources/views/vendor/voluum-affiliate'),
//        ], 'views');
//
//        $this->publishes([
//            __DIR__.'/../resources/assets' => base_path('resources/assets/vendor/voluum-affiliate'),
//        ], 'assets');

        $this->publishes([
            __DIR__.'/../resources/docs' => public_path('docs'),
        ], 'views');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register repositories interfaces bindings
        $this->app->bind(
            'VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository',
            'VoluumAffiliate\Database\Repositories\Implementations\RawAffiliateNetworksRepository'
        );
        $this->app->bind(
            'VoluumAffiliate\Database\Repositories\ITrafficSourcesRepository',
            'VoluumAffiliate\Database\Repositories\Implementations\RawTrafficSourcesRepository'
        );
        $this->app->bind(
            'VoluumAffiliate\Database\Repositories\IOffersRepository',
            'VoluumAffiliate\Database\Repositories\Implementations\RawOffersRepository'
        );

        // Register Internal Network Interface
        $this->app->bind(
            'VoluumAffiliate\Networks\IInternalNetworkApi',
            'VoluumAffiliate\Networks\Implementations\VoluumApi'
        );

        // Register Affiliate Network Interface
        $this->app->bind(
            'VoluumAffiliate\Networks\IAffiliateNetworkApi',
            'VoluumAffiliate\Networks\AffiliateNetworkApi'
        );

        // Register Services
        $this->app->singleton('VoluumAffiliate\Services\AffiliateNetworksService', function ($app) {
            return new AffiliateNetworksService(
                $app['VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository'],
                $app['VoluumAffiliate\Networks\IInternalNetworkApi']
            );
        });

        // Register commands
        $this->app->singleton('command.voluum_affiliate.update_offers', function($app) {
            return new UpdateOffers();
        });
        $this->commands('command.voluum_affiliate.update_offers');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository',
            'VoluumAffiliate\Database\Repositories\ITrafficSourcesRepository',
            'VoluumAffiliate\Database\Repositories\IOffersRepository',

            'VoluumAffiliate\Networks\IInternalNetworkApi',
            'VoluumAffiliate\Networks\IAffiliateNetworkApi',

            'VoluumAffiliate\Services\AffiliateNetworksService',

            'command.voluum_affiliate.update_offers'
        ];
    }
}
