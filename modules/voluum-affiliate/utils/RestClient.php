<?php

namespace VoluumAffiliate\Utils;
use Exception;
use stdClass;

/**
 * Class RestClient
 * @package VoluumAffiliate\Utils
 */
Class RestClient  {

    /**
     * @param string $method
     * @param $url
     * @param array $headers
     * @param array $data
     * @param mixed $auth
     * @return stdClass
     */
    public static function exec($method = 'GET', $url, $headers = array(), $data = array(), $auth = '') {

        // Check a url is provided or return 400 - bad request
        if (!$url) {
            return array('status' => 400, 'header' => null, 'data' => array('error' => 'No url provided for request'));
        }

        // Check if no Content type is defined so we force it
        if (!in_array('Content-Type', $headers)) {
            $headers['Content-Type'] = 'application/json';
        }

        // Check if no Accept is defined so we force it
        if (!in_array('Accept', $headers)) {
            $headers['Accept'] = 'application/json';
        }

        // Transform hash styled input array for headers intoa  plain one for Curl
        $aPlainHeaders = array();
        foreach($headers as $key => $value) {
            $aPlainHeaders[] = "${key}: ${value}";
        }

        // Init Curl
        $curl = curl_init();

        // Build payloads/queries depending on method type
        switch($method) {

            case 'POST':
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;

            case 'PUT':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;

            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;

            default: // GET
                if(!empty($data)) {
                    $url .= (strrpos($url, "?") ? '&' : '?') . http_build_query($data);
                }
        }

        // Check auth
        if (!empty($auth)) {

            switch($auth->type) {

                default: // Basic
                    if (isset($auth->credentials)) {
                        $basicAuth = $auth->credentials->user . ':' . $auth->credentials->password;
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_USERPWD, $basicAuth);
                    }
            }
        }

        // Set common Curl parameters
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $aPlainHeaders);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);

        // Try to execute
        try {
            // Execute Request
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            // Parse Response data
            $header = trim(substr($response, 0, $info['header_size']));
            $body = substr($response, $info['header_size']);

        // Fallback to 500 error code
        } catch (Exception $e) {
            $info = array('http_code' => 500);
            $header = '';
            $body = '{"error":{"message":"Error in Request"}}';
        }

        // Return custom response
        return (object) array('status' => $info['http_code'], 'header' => $header, 'data' => json_decode($body));
    }

    /**
     * @param $url
     * @param $headers
     * @param array $data
     * @param array $auth
     * @return array
     */
    public static function get($url, $headers, $data = array(), $auth = array()) {
        return RestClient::exec("GET", $url, $headers, $data, $auth);
    }

    /**
     * @param $url
     * @param $headers
     * @param array $data
     * @param array $auth
     * @return array
     */
    public static function post($url, $headers, $data = array(), $auth = array()) {
        return RestClient::exec("POST", $url, $headers, $data, $auth);
    }

    /**
     * @param $url
     * @param $headers
     * @param array $data
     * @param array $auth
     * @return array
     */
    public static function put($url, $headers, $data = array(), $auth = array()) {
        return RestClient::exec("PUT", $url, $headers, $data, $auth);
    }

    /**
     * @param $url
     * @param $headers
     * @param array $data
     * @param array $auth
     * @return array
     */
    public static function delete($url, $headers, $data = array(), $auth = array()) {
        return RestClient::exec("DELETE", $url, $headers, $data, $auth);
    }
}
