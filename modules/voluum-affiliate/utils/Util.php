<?php

namespace VoluumAffiliate\Utils;

use Log;
use Illuminate\Http\Request;

/**
 * Class Util
 * @package VoluumAffiliate\Utils
 */
class Util
{
    /** @var array $cost_models */
    public static $cost_models = array(
        'CPI',
        'CPA',
        'CPC'
    );

    /** @var array $platforms */
    public static $platforms = array(
        'Android',
        'iOS'
    );

    /** @var array $platformMap */
    public static $platformMap = array(
        'android' => 'Android',
        'ios' => 'iOS',
        'iphone' => 'iOS',
        'ipad' => 'iOS'
    );

    /**
     * @param string $text
     * @param string $severity
     * @param string $log
     * @return string
     */
    public static function multiLog($text = "", $severity = "info", $log = "")
    {
        Log::$severity($text);
        return $log . "<p>${text}</p>";
    }

    /**
     * @param string $html
     * @return mixed
     */
    public static function htmlLogToText($html = "")
    {
        $find = array("<p>", "</p>");
        $replacements = array("", "\n");
        $result = str_replace($find, $replacements, $html);
        return $result;
    }

    /**
     * @param Request $request
     * @param $result
     * @param string $defaultMessage
     */
    public static function setServiceFeedbackFlashMessages(Request $request, $result, $defaultMessage = "")
    {
        // Check unknown error
        if (empty($result)) {
            $request->session()->flash('message', array('severity' => 'danger', 'body' => $defaultMessage));
        }

        // Check service error
        if (isset($result['error']) && !empty($result['error']) && isset($result['error']['message']) && !empty($result['error']['message'])) {
            $request->session()->flash('message', array('severity' => 'danger', 'body' => $result['error']['message']));
        }

        // Check service output
        if (isset($result['output']) && !empty($result['output'])) {
            $request->session()->flash('message', array('severity' => 'info', 'body' => $result['output']));
        }
    }
}