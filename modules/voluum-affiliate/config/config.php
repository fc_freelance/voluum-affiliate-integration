<?php

return array(

    'api' => array(
        'endpoint' => env('VOLUUM_AFFILIATE_API_ENDPOINT', '<default voluum api endpoint here>'),
        'paths' => array(
            'campaigns' => 'campaigns'
        )
    ),
    'assets' => array(
        'local_path' => env('VOLUUM_AFFILIATE_ASSETS_LOCAL_PATH', '<default assets local path here>'),
        'base_url' => env('VOLUUM_AFFILIATE_ASSETS_BASE_URL', '<default assets base endpoint here>')
    ),
    'networks' => array(

        'voluum' => array(
            'auth' => array(
                'type' => 'basic',
                'url' => 'https://security.voluum.com/login',
                'credentials' => array(
                    'user' => env('VOLUUM_AFFILIATE_VOLUUM_AUTH_USER', 'no_voluum_user'),
                    'password' => env('VOLUUM_AFFILIATE_VOLUUM_AUTH_PASSWORD', 'no_voluum_password')
                )
            ),
            'endpoint' => 'https://core.voluum.com',
            'other_endpoints' => array(
                'reporting' => 'https://reports.voluum.com/report'
            ),
            'paths' => array(
                'offers' => 'offers',
                'campaigns' => 'campaigns',
                'traffic_sources' => 'traffic-sources',
                'affiliate_networks' => 'affiliate-networks'
            ),
            'defaults' => array(
                'all_countries_text' => 'Global'
            )
        ),

        'youappi' => array(
            'endpoint' => 'http://service.youappi.com',
            'paths' => array(
                'offers' => 'cmp/campaigninfo'
            ),
            'auth_token' => env('VOLUUM_AFFILIATE_YOUAPPI_ACCESS_TOKEN', 'no_youappi_token')
        ),

        'artofclick' => array(
            'endpoint' => 'http://api.artofclick.com/web/Api/v1.4',
            'paths' => array(
                'offers' => 'offer.json'
            ),
            'auth_token' => env('VOLUUM_AFFILIATE_ARTOFCLICK_ACCESS_TOKEN', 'no_artofclick_token')
        ),

        'clickky' => array(
            'endpoint' => 'https://cpactions.com/api/v1.0/feed/public',
            'paths' => array(
                'offers' => 'offers?site_id=<some_clickky site id here>'
            ),
            'auth_token' => env('VOLUUM_AFFILIATE_CLICKKY_ACCESS_TOKEN', 'no_clickky_token')
        )
    ),
    'defaults' => array(
        'offer_to_campaign_payout_multiplier' => 0.85
    )
);
