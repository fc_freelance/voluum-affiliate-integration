<?php

namespace VoluumAffiliate\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use VoluumAffiliate\Services\OffersService;
use VoluumAffiliate\Utils\Util;

/**
 * Class UpdateOffers
 * @package VoluumAffiliate\Jobs
 */
class UpdateOffers extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var OffersService $offersService */
    protected $offersService;

    /**
     * Create a new job instance.
     * @param OffersService $offersService
     */
    public function __construct(OffersService $offersService)
    {
        $this->offersService = $offersService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start_date = $date = date("Y-m-d H:i:s");
        $result =$this->offersService->updateOffers();
        $end_date = $date = date("Y-m-d H:i:s");
        echo "---------------------------------------------------\n";
        echo "${start_date} - Started Offers Update process\n";
        echo "---------------------------------------------------\n";
        echo Util::htmlLogToText($result);
        echo "---------------------------------------------------\n";
        echo "${end_date} - Finished Offers Update process\n";
        echo "---------------------------------------------------\n";
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
    }
}
