<?php

namespace VoluumAffiliate\Database\Repositories;

use VoluumAffiliate\Database\Repositories\IRepository;

/**
 * Interface IOffersRepository
 * @package VoluumAffiliate\Database\Repositories
 */
interface IOffersRepository extends IRepository
{
    /**
     * @param string $id
     * @return mixed
     */
    public function getByExternalId($id = '');

    /**
     * @param string $id
     * @return mixed
     */
    public function getByInternalId($id = '');
}