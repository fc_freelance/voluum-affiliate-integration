<?php

namespace VoluumAffiliate\Database\Repositories;

/**
 * Interface IRepository
 * @package VoluumAffiliate\Database\Repositories
 */
interface IRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function query($data = array());

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function get($id = '');

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = array());

    /**
     * @param string|integer $id
     * @param array $data
     * @return mixed
     */
    public function update($id = '', $data = array());

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function delete($id);
}