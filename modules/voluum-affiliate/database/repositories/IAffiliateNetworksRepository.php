<?php

namespace VoluumAffiliate\Database\Repositories;

use VoluumAffiliate\Database\Repositories\IRepository;

/**
 * Interface IAffiliateNetworksRepository
 * @package VoluumAffiliate\Database\Repositories
 */
interface IAffiliateNetworksRepository extends IRepository
{

}