<?php

namespace VoluumAffiliate\Database\Repositories;

use VoluumAffiliate\Database\Repositories\IRepository;

/**
 * Interface ITrafficSourcesRepository
 * @package VoluumAffiliate\Database\Repositories
 */
interface ITrafficSourcesRepository extends IRepository
{

}