<?php

namespace VoluumAffiliate\Database\Repositories\Implementations;

use DB;

use VoluumAffiliate\Database\Repositories\IOffersRepository;
use VoluumAffiliate\Database\Repositories\Implementations\RawRepository;

/**
 * Class RawOffersRepository
 * @package VoluumAffiliate\Database\Repositories\Implementations
 */
class RawOffersRepository extends RawRepository implements IOffersRepository
{
    /** @var string $table */
    protected $table = 'offers';

    /**
     * @param string $id
     * @return mixed
     */
    public function getByExternalId($id = '')
    {
        // Get collection from query
        $collection = $this->query(array('external_id' => $id));

        // No data found, return null
        if (empty($collection)) return null;

        // Return first element as it should be unique
        return $collection[0];
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getByInternalId($id = '')
    {
        // Get collection from query
        $collection = $this->query(array('internal_id' => $id));

        // No data found, return null
        if (empty($collection)) return null;

        // Return first element as it should be unique
        return $collection[0];
    }
}