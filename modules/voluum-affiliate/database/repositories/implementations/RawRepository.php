<?php

namespace VoluumAffiliate\Database\Repositories\Implementations;

use DB;
use Exception;
use Log;
use VoluumAffiliate\Database\Repositories\IRepository;

/**
 * Class RawRepository
 * @package VoluumAffiliate\Database\Repositories\Implementations
 */
class RawRepository implements IRepository
{
    /** @var string $table */
    protected $table = '';

    /**
     * @param array $data
     * @return mixed
     */
    public function query($data = array())
    {
        // Init query
        $q = "select * from {$this->table} where 1=1";

        // Append query parameters
        foreach($data as $key => $value) {
            $q .= " and ${key} = :${key}";
        }

        // Execute query
        try {
            $results = DB::select($q, $data);
        } catch (Exception $e) {
            Log::error("Error getting entries from {$this->table}");
            $results = null;
        }

        // Return results
        return $results;
    }

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function get($id = '')
    {
        // For now, just query this
        try {
            $result = $this->query(array('id' => $id));
        } catch (Exception $e) {
            Log::error("Error retrieving entry from {$this->table}");
            $result = null;
        }

        // And return first entry or null
        return $result ? $result[0] : null;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = array())
    {
        // Init query
        $q = "insert into {$this->table} (";

        // Append query parameters
        foreach($data as $key => $value) {
            $q .= "${key},";
        }

        $q = rtrim($q, ",") . ") values (";

        // Append query parameters
        foreach($data as $key => $value) {
            $q .= ":${key},";
        }

        $q = rtrim($q, ",") . ")";

        // Execute query
        try {
            $result = DB::insert($q, $data);
        } catch (Exception $e) {
            Log::error("Error storing entry into {$this->table}");
            $result = null;
        }

        return $result;
    }

    /**
     * @param string|integer $id
     * @param array $data
     * @return mixed
     */
    public function update($id = '', $data = array())
    {
        // Init query
        $q = "update {$this->table} set ";

        // Append query parameters, but remove id
        foreach($data as $key => $value) {
            if ($key != 'id') $q .= "${key} = :${key},";
        }

        $q = rtrim($q, ",") . " where id = :id";

        $data['id'] = $id;

        // Execute query
        try {
            $result = DB::update($q, $data);
        } catch (Exception $e) {
            Log::error("Error updating entry into {$this->table}");
            $result = null;
        }

        return $result;
    }

    /**
     * @param string|integer $id
     * @return mixed
     */
    public function delete($id)
    {
        // Execute query
        try {
            $result = DB::delete("delete from {$this->table} where id = :id", [$id]);
        } catch (Exception $e) {
            Log::error("Error deleting entry from {$this->table}");
            $result = null;
        }

        return $result;
    }
}