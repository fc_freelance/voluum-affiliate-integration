<?php

namespace VoluumAffiliate\Database\Repositories\Implementations;

use DB;

use VoluumAffiliate\Database\Repositories\ITrafficSourcesRepository;
use VoluumAffiliate\Database\Repositories\Implementations\RawRepository;

/**
 * Class RawTrafficSourcesRepository
 * @package VoluumAffiliate\Database\Repositories\Implementations
 */
class RawTrafficSourcesRepository extends RawRepository implements ITrafficSourcesRepository
{
    /** @var string $table */
    protected $table = 'traffic_sources';
}