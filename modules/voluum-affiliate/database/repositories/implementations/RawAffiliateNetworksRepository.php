<?php

namespace VoluumAffiliate\Database\Repositories\Implementations;

use DB;

use VoluumAffiliate\Database\Repositories\IAffiliateNetworksRepository;
use VoluumAffiliate\Database\Repositories\Implementations\RawRepository;

/**
 * Class RawAffiliateNetworksRepository
 * @package VoluumAffiliate\Database\Repositories\Implementations
 */
class RawAffiliateNetworksRepository extends RawRepository implements IAffiliateNetworksRepository
{
    /** @var string $table */
    protected $table = 'affiliate_networks';
}