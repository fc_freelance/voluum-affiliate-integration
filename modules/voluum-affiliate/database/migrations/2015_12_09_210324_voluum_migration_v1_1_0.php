<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class VoluumMigrationV110
 */
class VoluumMigrationV110 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('internal_id', 64)->unique();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('countries')->nullable();
            $table->string('platforms', 64)->nullable();
            $table->integer('min_caps')->unsigned()->nullable();
            $table->string('cost_model')->nullable();
            $table->boolean('incent')->nullable();
            $table->boolean('auto')->nullable();
            $table->boolean('auto_payout')->nullable();
        });

        Schema::table('affiliate_networks', function (Blueprint $table) {
            $table->boolean('operator_based')->nullable();
            $table->boolean('auto')->nullable();
            $table->boolean('auto_payout')->nullable();        });

        Schema::table('offers', function (Blueprint $table) {
            $table->string('preview_url')->nullable();
            $table->string('preview_image')->nullable();
            $table->string('platforms', 64)->nullable();
            $table->integer('caps')->unsigned()->nullable();
            $table->string('cost_model')->nullable();
            $table->boolean('incent')->nullable();
            $table->boolean('operator_based')->nullable();
            $table->string('creatives_url')->nullable();
            $table->boolean('auto')->nullable();
            $table->boolean('auto_payout')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function ($table) {
            $table->dropColumn([
                'preview_url',
                'preview_image',
                'platforms',
                'caps',
                'cost_model',
                'incent',
                'operator_based',
                'creatives_url',
                'auto',
                'auto_payout'
            ]);
        });

        Schema::table('affiliate_networks', function ($table) {
            $table->dropColumn([
                'operator_based',
                'auto',
                'auto_payout'
            ]);
        });

        Schema::drop('traffic_sources');
    }
}
