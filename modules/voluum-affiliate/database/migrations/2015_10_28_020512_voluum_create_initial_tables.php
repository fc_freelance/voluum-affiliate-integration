<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class VoluumCreateInitialTables
 */
class VoluumCreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_networks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('internal_id', 64)->unique();
            $table->string('network_code', 32)->unique()->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('internal_id', 64)->unique();
            $table->string('external_id')->nullable();
            $table->integer('affiliate_network_id')->unsigned();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::table('offers', function ($table) {
            $table->foreign('affiliate_network_id')->references('id')->on('affiliate_networks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offers');
        Schema::drop('affiliate_networks');
    }
}
