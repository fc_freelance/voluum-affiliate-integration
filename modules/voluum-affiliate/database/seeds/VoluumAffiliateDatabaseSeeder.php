<?php

use Illuminate\Database\Seeder;

class VoluumAffiliateDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoluumAffiliateNetworksSeeder::class);
        $this->call(VoluumTrafficSourcesSeeder::class);
    }
}
