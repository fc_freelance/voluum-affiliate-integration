<?php

namespace VoluumAffiliate\Networks;

use stdClass;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Models\TrafficSource;

/**
 * Interface IAffiliateNetworkApi
 * @package VoluumAffiliate\Networks
 */
interface IAffiliateNetworkApi extends INetworkApi
{
    /**
     * @return AffiliateNetwork
     */
    public function getAffiliateNetwork();
}