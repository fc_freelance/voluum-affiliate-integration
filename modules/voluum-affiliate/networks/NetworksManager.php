<?php

namespace VoluumAffiliate\networks;

use App;
use Log;

use VoluumAffiliate\Networks\Implementations\ArtOfClickApi;
use VoluumAffiliate\Networks\Implementations\ClickkyApi;
use VoluumAffiliate\Networks\Implementations\VoluumApi;
use VoluumAffiliate\Networks\Implementations\YouappiApi;
use VoluumAffiliate\Networks\INetworkApi;
use VoluumAffiliate\Services\AffiliateNetworksService;

/**
 * Class NetworksManager
 * @package VoluumAffiliate\networks
 */
class NetworksManager {

    /** @const string NO_API_NETWORK_CODE */
    const NO_API_NETWORK_CODE = '_none';

    /** @const string INTERNAL_NETWORK */
    const INTERNAL_NETWORK = 'voluum';

    /** @const integer ITEMS_PER_PAGE */
    const ITEMS_PER_PAGE = 50;

    /**
     * @param $network
     * @return INetworkApi
     */
    public static function getNetworkApi($network) {

        /** @var INetworkApi $networkApi */
        $networkApi = null;

        // Assing API depending on our internal code (in config file networks key)
        switch($network) {

            // Internal Network (Voluum right now)
            case static::INTERNAL_NETWORK:
                $networkApi = new VoluumApi();
                break;

            // Youappi Affiliate Network
            case 'youappi':
                $networkApi = new YouappiApi(App::make('VoluumAffiliate\Services\AffiliateNetworksService'));
                break;

            // ArtOfClick Affiliate Network
            case 'artofclick':
                $networkApi = new ArtOfClickApi(App::make('VoluumAffiliate\Services\AffiliateNetworksService'));
                break;

            // Clickky Affiliate Network
            case 'clickky':
                $networkApi = new ClickkyApi(App::make('VoluumAffiliate\Services\AffiliateNetworksService'));
                break;

            // As default, show message and continue
            default:
                Log::warning("Tries to get non supported network: $network");
        }

        return $networkApi;
    }

    /**
     * @return INetworkApi
     */
    public static function getInternalApi() {

        // Return the network defined for the internal Api
        return static::getNetworkApi(static::INTERNAL_NETWORK);
    }
}