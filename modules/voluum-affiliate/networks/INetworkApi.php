<?php

namespace VoluumAffiliate\Networks;

use stdClass;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;

/**
 * Interface INetworkApi
 * @package VoluumAffiliate\Networks
 */
interface INetworkApi
{
    /**
     * @param string $path
     * @param null $default
     * @return mixed
     */
    public function getConfig($path = '', $default = null);

    /**
     * @param array $data
     * @return mixed
     */
    public function listOffers($data = array());

    /**
     * @param string $id
     * @param array $data
     * @return mixed
     */
    public function getOffer($id, $data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function createOffer($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function updateOffer($data = array());

    /**
     * @param string $id
     * @return mixed
     */
    public function deleteOffer($id);

    /**
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array());

    /**
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array());

    /**
     * @param Offer $data
     * @return array
     */
    public function offerToPayload(Offer $data);
}