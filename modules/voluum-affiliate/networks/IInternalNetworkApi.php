<?php

namespace VoluumAffiliate\Networks;

use stdClass;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Models\TrafficSource;

/**
 * Interface IInternalNetworkApi
 * @package VoluumAffiliate\Networks
 */
interface IInternalNetworkApi extends INetworkApi
{
    /**
     * @param Offer|null $iOffer
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function mergeInternalOfferWithOffer($iOffer, $offer);

    /**
     * @param array $data
     * @return mixed
     */
    public function listCampaigns($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function getReport($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function getLiveReport($data = array());

    /**
     * @param Offer $offer
     * @param TrafficSource $source
     * @param int $payout_multiplier
     * @return array|null
     */
    public function generateCampaignDataFromOffer(Offer $offer, TrafficSource $source, $payout_multiplier = 1);

    /**
     * @param Offer $offer
     * @param TrafficSource $traffic_source
     * @param int $payout_multiplier
     * @return mixed
     */
    public function createCampaignFromOffer($offer, $traffic_source, $payout_multiplier = 1);

    /**
     * @param array $data
     * @return mixed
     */
    public function createCampaign($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function updateCampaign($data = array());

    /**
     * @param string $id
     * @return mixed
     */
    public function deleteCampaign($id);

    /**
     * @param string $id
     * @return mixed
     */
    public function recoverCampaign($id);

    /**
     * @param array|stdClass $data
     * @return array
     */
    public function campaignsReportToPayload($data = array());

    /**
     * @param array $data
     * @return array
     */
    public function reportCampaignsToPayload($data = array());

    /**
     * @param array $data
     * @return array
     */
    public function reportCampaignToPayload($data = array());

    /**
     * @param string $id
     * @return mixed
     */
    public function recoverOffer($id);

    /**
     * @param array $data
     * @return array
     */
    public function reportOffersToPayload($data = array());

    /**
     * @param array|stdClass $data
     * @return array
     */
    public function reportOfferToPayload($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function listTrafficSources($data = array());

    /**
     * @param string $id
     * @return null|stdClass
     */
    public function getTrafficSource($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function updateTrafficSource($data = array());

    /**
     * @param array $data
     * @return array
     */
    public function payloadToTrafficSources($data = array());

    /**
     * @param array|stdClass $data
     * @return TrafficSource
     */
    public function payloadToTrafficSource($data = array());

    /**
     * @param array $data
     * @return mixed
     */
    public function listAffiliateNetworks($data = array());

    /**
     * @param string $id
     * @return null|stdClass
     */
    public function getAffiliateNetwork($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function updateAffiliateNetwork($data = array());

    /**
     * @param array $data
     * @return array
     */
    public function payloadToAffiliateNetworks($data = array());

    /**
     * @param array|stdClass $data
     * @return AffiliateNetwork
     */
    public function payloadToAffiliateNetwork($data = array());
}