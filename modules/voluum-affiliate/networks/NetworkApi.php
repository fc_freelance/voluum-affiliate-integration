<?php

namespace VoluumAffiliate\Networks;

use Log;
use stdClass;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Utils\RestClient;
use VoluumAffiliate\Networks\INetworkApi;

/**
 * Class NetworkApi
 * @package VoluumAffiliate\Networks
 */
abstract class NetworkApi implements INetworkApi {

    /** @var null|string $network  */
    protected $network = null;

    /** @var null|array $config */
    protected $config = null;

    /** @var null|array $auth_token  */
    protected $auth_token = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        /** @var array $config */
        $config = config('voluum-affiliate');

        // Get full config as object!!
        $this->config = json_decode(json_encode($config['networks'][$this->network]));
    }

    /**
     * @param string $path
     * @param null $default
     * @return mixed
     */
    public function getConfig($path = '', $default = null)
    {
        // Explode path
        $aPath = explode('.', $path);

        // Hold ref to config
        $config = $this->config;

        // Loop over keys
        foreach ($aPath as $key) {
            if (!isset($config->{$key})) return $default;
            $config = $config->{$key};
        }

        // Return final result
        return $config;
    }

    /**
     * Tries to authenticate against API
     *
     * @param array $data
     * @return mixed
     */
    protected function auth($data = array())
    {
        // If no auth set, assume we have in config the auth token
        if (!isset($this->config->auth)) {

            // Directly set it
            $this->auth_token = $this->config->auth_token;

        // Auth set, proceed
        } else {

            // Build the url
            $url = $this->config->auth->url ;

            // Check if data provided or default to credentials
            if (empty($data)) {
                $data = $this->config->auth;
            }

            // Call API
            $res = RestClient::get($url, array(), array(), $data);

            // Check if success
            $success = isset($res->status) && ($res->status == 200);

            // If success, store the auth token
            if ($success) {
                $this->auth_token = $this->getAuthTokenFromResponse($res);
            }
        }

        // Return token
        return $this->auth_token;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function listOffers($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $data);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting offers from ' . $this->network);
            return array();
        }

        // Parse offers from response
        $offers = $res->data;

        // Return response
        return $offers;
    }

    /**
     * @param string $id
     * @param array $data
     * @return mixed
     */
    public function getOffer($id = '', $data = array())
    {
        // No id, no data
        if (empty($id)) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers  . '/' . $id);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders());

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting offer ' . $id . ' from ' . $this->network);
            return null;
        }

        // Parse offer from response
        $offer = $res->data;

        // Return response
        return $offer;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createOffer($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers);

        // Call API
        $res = RestClient::post($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function updateOffer($data = array())
    {
        // No id, no data
        if (empty($data) || !isset($data['id'])) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers . '/' . $data['id']);

        // Call API
        $res = RestClient::put($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function deleteOffer($id = '')
    {
        // No id, no data
        if (empty($id)) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers  . '/' . $id);

        // Call API
        $res = RestClient::delete($url, $this->getAuthHeaders());

        // Return response
        return $res;
    }

    /**
     * Tries to parse the response data from auth request and return the token
     *
     * @param array $res
     * @return null|string
     */
    protected function getAuthTokenFromResponse($res = array())
    {
        // Defaults to nothing
        return null;
    }

    /**
     * Gets auth headers for API requests
     *
     * @param array $data
     * @return array
     */
    protected function getAuthHeaders($data = array())
    {
        // Defaults to nothing
        return array();
    }

    /**
     * Gets url with auth appended, if any
     *
     * @param string $url
     * @return string
     */
    protected function getUrlAuth($url = '')
    {
        // Defaults to nothing
        return $url;
    }

    /**
     * Parses response payload to offers
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array())
    {
        // Init array
        $offers = array();

        // Loop over collection
        foreach($data as $key => $value) {

            // Get parsed offer
            $offer = $this->payloadToOffer($value);

            // If null, continue to next
            if (empty($offer)) continue;

            // Add to collection
            $offers[] = $offer;
        }
        // Return them
        return $offers;
    }

    /**
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Defaults to directly parse it
        return new Offer($data);
    }

    /**
     * @param Offer $data
     * @return array
     */
    public function offerToPayload(Offer $data)
    {
        // Defaults to directly parse it
        return (array) $data;
    }
}