<?php

namespace VoluumAffiliate\Networks;

use App;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Services\AffiliateNetworksService;


/**
 * Class AffiliateNetworkApi
 * @package VoluumAffiliate\Networks
 */
abstract class AffiliateNetworkApi extends NetworkApi implements IAffiliateNetworkApi
{
    /** @var AffiliateNetworksService */
    protected $networkService;

    /** @var  AffiliateNetwork */
    protected $affiliate_network;

    /**
     * @Override
     *
     * Constructor
     * @param AffiliateNetworksService $networksService
     */
    public function __construct(AffiliateNetworksService $networksService)
    {
        // Call parent first
        parent::__construct(App::make('VoluumAffiliate\Services\OffersService'));

        // Assign service
        $this->networkService = $networksService;

        // Assign affiliate Network
        $this->affiliate_network = new AffiliateNetwork($this->networkService->getByCode($this->network));
    }

    /**
     * @return AffiliateNetwork
     */
    public function getAffiliateNetwork()
    {
        return $this->affiliate_network;
    }
}