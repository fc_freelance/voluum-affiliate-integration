<?php

namespace VoluumAffiliate\Networks\Implementations;

use stdClass;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Networks\AffiliateNetworkApi;
use VoluumAffiliate\Utils\Util;

/**
 * Class YouappiApi
 * @package VoluumAffiliate\Networks
 */
class YouappiApi extends AffiliateNetworkApi {

    /** @var string $network */
    protected $network = 'youappi';

    /**
     * @Override
     * Gets url with auth appended, if any
     *
     * @param string $url
     * @return string
     */
    protected function getUrlAuth($url = '')
    {
        // Put as first or other qs param
        $url .= strrpos($url, "?") ? '&' : '?';

        // Append token to url
        return $url . 'accesstoken=' . $this->auth_token;
    }

    /**
     * @Override
     *
     * @param string $id
     * @param array $data
     * @return mixed
     */
    public function getOffer($id = '', $data = array())
    {
        // Get all offers first
        $offers = $this->payloadToOffers($this->listOffers());

        // Now filter this one
        $filtered = array_values(array_filter($offers['offers'], function(Offer $entry) use ($id) {
            return $entry->getExternalId() == $id;
        }));

        // Return found or null
        return count($filtered) ? $filtered[0] : null;
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array())
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Check input
        if (empty($data) || !isset($data->data)) return array('offers' => array());

        // Call parent with fixed data
        $offers = parent::payloadToOffers($data->data);

        // Return with pages if any
        return array(
            'offers' => $offers
        );
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array())
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Get underlying network
        $network = $this->getAffiliateNetwork();

        // Form tracking url
        $url = $data->redirect_url;
        $url = preg_replace('/subid=/', 'subid={clickid}', $url);
        $url = preg_replace('/publishertoken=/', 'publishertoken={trafficsource.id}', $url);

        // Prepare platforms
        $platform = (isset($data->platform) && $data->platform) ? strtolower($data->platform) : null;
        if (empty($platform)) {
            $platforms = array();
        } else {
            $platforms = array(Util::$platformMap[$platform]);
        }

        $countries = array();
        if (isset($data->countries)) {
            $countries = array_map(function($entry){
                return strtoupper($entry);
            }, $data->countries);
        }

        // TODO: Better way to handle this weird object from API??
        $creatives_url = null;
        if (isset($data->creatives) && !empty($data->creatives)) {
            foreach ($data->creatives as $key => $value) {
                $creatives_url = $value;
                break;
            }
        }

        // Get proper data from input
        $parsedData = array(
            'external_id' => $data->campaign_id,
            'payout' => isset($data->cpi) ? $data->cpi : (isset($data->cpc) ? $data->cpc : null),
            'countries' => $countries,
            'url' => $url,
            'source_name' => (isset($data->app_details) && isset($data->app_details->app_name)) ? $data->app_details->app_name : '',
            'preview_url' => isset($data->app_url) ? $data->app_url : '',
            'preview_image' => (isset($data->app_details) && isset($data->app_details->app_icon)) ? $data->app_details->app_icon : '',
            'platforms' => $platforms,
            'cost_model' => isset($data->cpi) ? 'CPI' : (isset($data->cpc) ? 'CPC' : null),
            'caps' => (isset($data->capping_type) && $data->capping_type == 'installs' && isset($data->max_daily)) ? $data->max_daily : null,
            'creatives_url' => $creatives_url,
            'incent' => false,
            'operator_based' => $network->isOperatorBased(),
            'auto' => $network->isAuto(),
            'auto_payout' => $network->isAutoPayout()
        );

        // Call parent with fixed data
        return parent::payloadToOffer($parsedData);
    }
}