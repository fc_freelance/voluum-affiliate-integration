<?php

namespace VoluumAffiliate\Networks\Implementations;

use DateTime;
use Log;
use stdClass;
use VoluumAffiliate\Models\AffiliateNetwork;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Models\TrafficSource;
use VoluumAffiliate\Networks\IInternalNetworkApi;
use VoluumAffiliate\Networks\NetworkApi;
use VoluumAffiliate\Utils\RestClient;

/**
 * Class VoluumApi
 * @package VoluumAffiliate\Networks
 */
class VoluumApi extends NetworkApi implements IInternalNetworkApi {

    /** @var string $network */
    protected $network = 'voluum';

    /**
     * @Override
     * Tries to parse the response data from auth request and return the token
     *
     * @param array $res
     * @return null|string
     */
    protected function getAuthTokenFromResponse($res = array())
    {
        // Check if relevant fields are in response, otherwise return null
        if (!isset($res->data) || !isset($res->data->token)) {
            return null;
        };

        // Return token from response data
        return $res->data->token;
    }

    /**
     * @Override
     * Gets auth headers for API requests
     *
     * @param array $data
     * @return array
     */
    protected function getAuthHeaders($data = array())
    {
        // Check if data provided or default to credentials
        if (empty($data)) {
            $data = array('token' => $this->auth_token);
        }

        // Return auth headers
        return array('cwauth-token' => $data['token']);
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array())
    {
        // Call parent with fixed data
        $offers = parent::payloadToOffers($data);

        // Return with pages if any
        return array(
            'offers' => $offers
        );
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // FIXME: The convention is to ALWAYS use an affiliate network in Voluum, otherwise skip this!!
        if (!isset($data->affiliateNetwork)) {
            return null;
        }

        // Get proper data from input
        $parsedData = array(
            'internal_id' => $data->id,
            'payout' => isset($data->payout) ? $data->payout : null,
            'name' => $data->namePostfix,
            'url' => $data->url,
            'internal_network_id' => $data->affiliateNetwork->id,
            'country' => isset($data->country->code) ? $data->country->code : null
        );

        // Call parent with fixed data
        return parent::payloadToOffer($parsedData);
    }

    /**
     * @param Offer|null $iOffer
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function mergeInternalOfferWithOffer($iOffer, $offer)
    {
        // No offer, return just internal
        if (empty($offer)) return $iOffer;

        // Check if got internal
        if (!empty($iOffer)) {

            // Set values
            $offer->setInternalId($iOffer->getInternalId());
            $offer->setPayout($iOffer->getPayout());
            $offer->setName($iOffer->getName());
            $offer->setUrl($iOffer->getUrl());
            $offer->setInternalNetworkId($iOffer->getInternalNetworkId());
            $offer->setCountry($iOffer->getCountry());
        }

        // Return the offer
        return $offer;
    }

    /**
     * @Override
     * @param Offer $data
     * @return array
     */
    public function offerToPayload(Offer $data)
    {
        $payload = array(
            'affiliateNetwork' => array(
                'id' => $data->getInternalNetworkId()
            ),
            'payout' => $data->getPayout(),
            'namePostfix' => $data->getComposedName(),
            'overridePayout' => true,
            'url' => $data->getUrl()
        );

        if ($id = $data->getInternalId()) {
            $payload['id'] = $id;
        }

        if ($country = $data->getCountry()) {
            $payload['country'] = array('code' => $country);
        }

        return $payload;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function listCampaigns($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $data);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting campaigns from ' . $this->network);
            return array();
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getReport($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $from = date("Y-m-d\T00:00:00\Z", strtotime('-1 day'));
        $to = date("Y-m-d\T00:00:00\Z");
        $url = $this->getUrlAuth($this->config->other_endpoints->reporting);

        // Build the qs
        $qData = array(
            'from' => isset($data['from']) ? $data['from'] : $from,
            'to' => isset($data['to']) ? $data['to'] : $to,
            'tz' => isset($data['tz']) ? $data['tz'] : 'Europe/Berlin',
            'sort' => isset($data['sort_by']) ? $data['sort_by'] : 'visits',
            'direction' => isset($data['sort_direction']) ? $data['sort_direction'] : 'desc',
            'groupBy' => isset($data['group_by']) ? $data['group_by'] : 'campaign',
            'include' => isset($data['include']) ? $data['include'] : 'all',
            'offset' => isset($data['offset']) ? $data['offset'] : 0,
            'limit' => isset($data['limit']) ? $data['limit'] : 1000000
//            'columns' => ''
        );

        // TODO: This is annoying, as this makes to only appear those items with visits
//        // Filter by traffic source if got publisher id
//        if (isset($data['publisher_id'])) {
//            $qData['filter1'] = 'traffic-source';
//            $qData['filter1Value'] = $data['publisher_id'];
//        }

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $qData);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting report from ' . $this->network);
            return array();
        }

        // Parse data from response
        $payload = $res->data;

        // Filter by traffic source if got traffic source name
        if (isset($data['traffic_source_name'])) {

            // Filter campaigns for the required source
            $payload->rows = array_values(array_filter($payload->rows, function($entry) use ($data) {
                // TODO: Achtung!!! We're matching agains name!! The reason: Voluum reporting API does not offer this id and name is unique ....
                return $entry->trafficSource == $data['traffic_source_name'];
            }));
        }

        // Return it
        return $payload;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getLiveReport($data = array())
    {
        // Check we got campaign
        // TODO: In the future maybe we have live reports for everything, we should decouple this then ...
        if (!isset($data['campaign_id'])) return null;

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Default live report type
        if (!isset($data['type'])) {
            $data['type'] = 'visits';
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->other_endpoints->reporting.'/live/'.$data['type'].'/'.$data['campaign_id']);

        // Build the qs
        $qData = array(
            'tz' => isset($data['tz']) ? $data['tz'] : 'Europe/Berlin',
        );

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $qData);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting live report from ' . $this->network);
            return array();
        }

        // Return it
        return $res->data;
    }

    /**
     * @param Offer $offer
     * @param TrafficSource $source
     * @param int $payout_multiplier
     * @return array|null
     */
    public function generateCampaignDataFromOffer(Offer $offer, TrafficSource $source, $payout_multiplier = 1)
    {
        // No data, return null
        if (empty($offer) || empty($source)) return null;

        // Build payload
        $payload = array(
            'clickRedirectType' => 'REGULAR',
            'costModel' => in_array($offer->getCostModel(), array('CPA', 'CPI')) ? 'CPA' : $offer->getCostModel(),
            'namePostfix' => $offer->getComposedName(),
            'paths' => array(array(
                'directLinking' => true,
                'weight' => '100',
                'active' => true,
                'offers' => array(array('offer' => array('id' => $offer->getInternalId()), 'weight' => '100'))
            )),
            'realtimeRoutingAPI' => 'DISABLED',
            'redirectTarget' => 'INLINE',
            'trafficSource' => array('id' => $source->getInternalId())
        );

        // Set proper payout
        switch ($payload['costModel']) {

            // CPA/CPI
            case 'CPA':
            case 'CPI':
                $payload['cpa'] = $payout_multiplier * $offer->getPayout();
                break;

            // CPC
            case 'CPC':
                // TODO: Cover this case in the future
                $payload['cpc'] = 0.01;
                break;
        }

        // Set country if any
        if ($country = $offer->getCountry()) {
            $payload['country'] = array('code' => $country);
        }

        // Return data
        return $payload;
    }

    /**
     * @param Offer $offer
     * @param TrafficSource $traffic_source
     * @param int $payout_multiplier
     * @return mixed
     */
    public function createCampaignFromOffer($offer, $traffic_source, $payout_multiplier = 1)
    {
        // No data, return null
        if (empty($offer) || empty($traffic_source)) return null;

        // Build payload
        $payload = $this->generateCampaignDataFromOffer($offer, $traffic_source, $payout_multiplier);

        // Return response
        return $this->createCampaign($payload);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createCampaign($data = array())
    {
        // No data, return null
        if (empty($data)) return null;

        // Check if object, then transform it
        if (is_object($data)) {
            $data = (array) $data;
        }

        // No auth, ... null
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns);

        // Call API
        $res = RestClient::post($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function updateCampaign($data = array())
    {
        // Check if object, then tranform it
        if (is_object($data)) {
            $data = (array) $data;
        }

        // No id, no data
        if (empty($data) || !isset($data['id'])) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns . '/' . $data['id']);

        // Unset unwanted properties
        unset($data['inlineFlow']);
        unset($data['updatedTime']);

        // Call API
        $res = RestClient::put($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function deleteCampaign($id)
    {
        // No id, no data
        if (empty($id)) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns  . '/' . $id);

        // Call API
        $res = RestClient::delete($url, $this->getAuthHeaders());

        // Return response
        return $res;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function recoverCampaign($id)
    {
        // No id, no data
        if (empty($id)) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns  . '/' . $id . '/restore');

        // Call API
        $res = RestClient::post($url, $this->getAuthHeaders());

        // Return response
        return $res;
    }

    /**
     * @param array|stdClass $data
     * @return array
     */
    // FIXME: Remove this if adopting the regular API rather than report one is OK (so currently we don't use this)
    public function campaignsReportToPayload($data = array())
    {
        // Get campaigns
        $campaigns = $this->reportCampaignsToPayload($data->rows);

        // Loop over them and set/unset fields for API
        $campaigns = array_map(function($entry) {

            // Unset traffic source name
            unset($entry['trafficSourceName']);

            // Get Campaign from regular API
            $apiCampaign = $this->getCampaign($entry['id']);

            // Get reference to paths
            $cPaths = $apiCampaign->paths;

            // Check there's ony ONE path
            if (!empty($cPaths) && count($cPaths) == 1) {

                // Get reference to path offers
                $pOffers = $cPaths[0]->offers;

                // Check that only ONE offer is set in the ONLY path
                if (!empty($pOffers) && count($pOffers) == 1) {

                    // Set the offer from path
                    $entry['offer_id'] = $pOffers[0]->offer->id;
                }
            }

            // Include entry in collection
            return $entry;

        }, $campaigns);

        // Now filter those without offer id
        $campaigns = array_values(array_filter($campaigns, function($entry) {
           return isset($entry['offer_id']);
        }));

        // Return adapted collection
        return $campaigns;
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportCampaignsToPayload($data = array())
    {
        // Init collection
        $parsedData = array();

        // Loop over campaigns
        foreach($data as $c) {

            // Adapt payload
            $parsedData[] = $this->reportCampaignToPayload($c);
        }
        // Return them
        return $parsedData;
    }

    /**
     * @param array|stdClass $data
     * @return array
     */
    public function reportCampaignToPayload($data = array())
    {
        if (empty($data)) return array();

        if (!is_object($data)) $data = (object) $data;

        // Get the cost model
        $cm = strtolower($data->costModel);

        // Init payout
        $payout = null;

        // Check if defined the value, then set payout
        if(isset($data->{$cm})) {
            $payout = $data->{$cm};
        }

        // Adapt payload
        $parsedData = array(
            'id' => $data->campaignId,
            'name' => $data->campaignNamePostfix,
            'active' => !$data->deleted,
            'country' => $data->campaignCountry,
            'url' => $data->campaignUrl,
            'costModel' => $data->costModel,
            'payout' => $payout,
            'postbackUrl' => $data->postbackUrl,
            'pixelUrl' => $data->pixelUrl,
            'created' => $data->created,
            'updated' => $data->updated,
            'trafficSourceName' => $data->trafficSource
        );

        // Return them
        return $parsedData;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function recoverOffer($id)
    {
        // No id, no data
        if (empty($id)) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->offers  . '/' . $id . '/restore');

        // Call API
        $res = RestClient::post($url, $this->getAuthHeaders());

        // Return response
        return $res;
    }

    /**
     * @param array $data
     * @return array
     */
    public function reportOffersToPayload($data = array())
    {
        // Init collection
        $parsedData = array();

        // Loop over campaigns
        foreach($data as $c) {

            // Adapt payload
            $parsedData[] = $this->reportOfferToPayload($c);
        }
        // Return them
        return $parsedData;
    }

    /**
     * @param array|stdClass $data
     * @return array
     */
    public function reportOfferToPayload($data = array())
    {
        if (empty($data)) return array();

        if (!is_object($data)) $data = (object) $data;

        // Adapt payload
        $parsedData = array(
            'id' => $data->offerId,
            'name' => $data->offerName,
            'active' => !$data->deleted,
            'country' => $data->offerCountry,
            'url' => $data->offerUrl,
            'payout' => $data->payout,
            'created' => $data->created,
            'updated' => $data->updated,
            'affiliateNetworkName' => $data->affiliateNetworkName
        );

        // Return them
        return $parsedData;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function listTrafficSources($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->traffic_sources);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $data);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting traffic sources from ' . $this->network);
            return array();
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }

    /**
     * @param string $id
     * @return null|stdClass
     */
    public function getTrafficSource($id)
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->traffic_sources . '/' . $id);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders());

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting traffic source: ' . $id);
            return null;
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function updateTrafficSource($data = array())
    {
        // Check if object, then transform it
        if (is_object($data)) {
            $data = (array) $data;
        }

        // No id, no data
        if (empty($data) || !isset($data['id'])) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->traffic_sources . '/' . $data['id']);

        // Unset unwanted properties
        unset($data['updatedTime']);

        // Call API
        $res = RestClient::put($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param array $data
     * @return array
     */
    public function payloadToTrafficSources($data = array())
    {
        // Init array
        $collection = array();

        // Only if data
        if (!empty($data)) {

            // Loop over collection
            foreach ($data as $key => $value) {

                // Get parsed item
                $item = $this->payloadToTrafficSource($value);

                // If null, continue to next
                if (empty($item)) continue;

                // Add to collection
                $collection[] = $item;
            }
        }

        // Return them
        return $collection;
    }

    /**
     * @param array|stdClass $data
     * @return TrafficSource
     */
    public function payloadToTrafficSource($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Get proper data from input
        $parsedData = array(
            'internal_id' => $data->id,
            'name' => $data->name,
            'postback_url' => $data->postbackUrl,
            'api_endpoint' => config('voluum-affiliate.api.endpoint').'/'.config('voluum-affiliate.api.paths.campaigns').'?publisher_id='.$data->id
        );

        // Create object
        return  new TrafficSource($parsedData);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function listAffiliateNetworks($data = array())
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->affiliate_networks);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders(), $data);

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting affiliate networks from ' . $this->network);
            return array();
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }

    /**
     * @param string $id
     * @return null|stdClass
     */
    public function getAffiliateNetwork($id)
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->affiliate_networks . '/' . $id);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders());

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting affiliate network: ' . $id);
            return null;
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }
        /**
     * @param array $data
     * @return mixed
     */
    public function updateAffiliateNetwork($data = array())
    {
        // Check if object, then transform it
        if (is_object($data)) {
            $data = (array) $data;
        }

        // No id, no data
        if (empty($data) || !isset($data['id'])) {
            return null;
        }

        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->affiliate_networks . '/' . $data['id']);

        // Unset unwanted properties
        unset($data['updatedTime']);

        // Call API
        $res = RestClient::put($url, $this->getAuthHeaders(), $data);

        // Return response
        return $res;
    }

    /**
     * @param array $data
     * @return array
     */
    public function payloadToAffiliateNetworks($data = array())
    {
        // Init array
        $collection = array();

        // Only if data is set
        if (!empty($data)) {

            // Loop over collection
            foreach ($data as $key => $value) {

                // Get parsed item
                $item = $this->payloadToAffiliateNetwork($value);

                // If null, continue to next
                if (empty($item)) continue;

                // Add to collection
                $collection[] = $item;
            }
        }

        // Return them
        return $collection;
    }

    /**
     * @param array|stdClass $data
     * @return AffiliateNetwork
     */
    public function payloadToAffiliateNetwork($data = array())
    {
        // Check input
        if (empty($data)) return null;

        // Get proper data from input
        $parsedData = array(
            'internal_id' => $data->id,
            'name' => $data->name
        );

        // Defaults to directly parse it
        return new AffiliateNetwork($parsedData);
    }

    /**
     * @param string $id
     * @return null|stdClass
     */
    public function getCampaign($id)
    {
        // No auth, no data
        if (!$this->auth_token && !$this->auth()) {
            return null;
        }

        // Build the url
        $url = $this->getUrlAuth($this->config->endpoint . '/' . $this->config->paths->campaigns . '/' . $id);

        // Call API
        /** @var stdClass $res */
        $res = RestClient::get($url, $this->getAuthHeaders());

        // Check if not 200
        if ($res->status !== 200) {
            Log::error('Error getting campaigns: ' . $id);
            return null;
        }

        // Parse data from response
        $payload = $res->data;

        // Return it
        return $payload;
    }
}