<?php

namespace VoluumAffiliate\Networks\Implementations;

use stdClass;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Networks\AffiliateNetworkApi;
use VoluumAffiliate\Utils\Util;

/**
 * Class ArtOfClickApi
 * @package VoluumAffiliate\Networks
 */
class ArtOfClickApi extends AffiliateNetworkApi {

    /** @var string $network */
    protected $network = 'artofclick';

    /**
     * @Override
     * Gets url with auth appended, if any
     *
     * @param string $url
     * @return string
     */
    protected function getUrlAuth($url = '')
    {
        // Put as first or other qs param
        $url .= strrpos($url, "?") ? '&' : '?';

        // Append token to url
        return $url . 'api_key=' . $this->auth_token;
    }

    /**
     * @Override
     *
     * @param string $id
     * @param array $data
     * @return mixed
     */
    public function getOffer($id = '', $data = array())
    {
        // Get all offers first
        $offers = $this->payloadToOffers($this->listOffers());

        // Now filter this one
        $filtered = array_values(array_filter($offers['offers'], function(Offer $entry) use ($id) {
            return $entry->getExternalId() == $id;
        }));

        // Return found or null
        return count($filtered) ? $filtered[0] : null;
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array())
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Check input
        if (empty($data) || !isset($data->response) || !isset($data->response->offers)) return array('offers' => array());

        // This API saves offer status, so make sure here they're still active
        $offers = array_values(array_filter($data->response->offers, function($entry) {
            return $entry->status === 'active';
        }));

        // Call parent with fixed data
        $offers = parent::payloadToOffers($offers);

        // Return it with pages if any
        return array(
            'offers' => $offers
        );
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array())
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Get underlying network
        $network = $this->getAffiliateNetwork();

        // Form tracking url
        $url = $data->tracking_url;
        $url = preg_replace('/aff_sub=[^$&]*/', 'aff_sub={clickid}', $url);
        $url = preg_replace('/source=[^$&]*/', 'source={trafficsource.id}', $url);

        // Prepare platforms
        $os = (isset($data->os) && $data->os) ? strtolower($data->os) : null;
        if (empty($os)) {
            $devices = (isset($data->device) && $data->device) ? explode(', ', $data->device) : array();
            $platforms = array();
            foreach ($devices as $device) {
                $dev = Util::$platformMap[strtolower($device)];
                if (!in_array($dev, $platforms)) {
                    $platforms[] = $dev;
                }
            }
        } else {
            $platforms = array_map(function($entry)  {
                return Util::$platformMap[$entry];
            }, explode(', ', $os));
        }

        // Get proper data from input
        $parsedData = array(
            'external_id' => $data->id,
            'payout' => $data->payout,
            // This comes as a comma separated list
            'countries' => isset($data->countries) ? explode(", ", $data->countries) : array(),
            'url' => $url,
            'source_name' => isset($data->name) ? $data->name : '',
            'preview_url' => isset($data->previewURL) ? $data->previewURL : '',
            'preview_image' => '',
            // TODO: Prefill with all available if empty??
            'platforms' => $platforms,
            'cost_model' => isset($data->payout_type) ? strtoupper($data->payout_type) : null,
            'caps' => isset($data->daily_cap) ? intval($data->daily_cap) : null,
            // TODO: So an array of them, getting first one for now
            'creatives_url' => (isset($data->creatives) && count($data->creatives)) ? $data->creatives[0] : null,
            'incent' => isset($data->incent) ? !!intval($data->incent) : false,
            'operator_based' => $network->isOperatorBased(),
            'auto' => $network->isAuto(),
            'auto_payout' => $network->isAutoPayout()
        );

        // Call parent with fixed data
        return parent::payloadToOffer($parsedData);
    }
}