<?php

namespace VoluumAffiliate\Networks\Implementations;

use stdClass;
use VoluumAffiliate\Models\Offer;
use VoluumAffiliate\Networks\AffiliateNetworkApi;
use VoluumAffiliate\networks\NetworksManager;

/**
 * Class ClickkyApi
 * @package VoluumAffiliate\Networks
 */
class ClickkyApi extends AffiliateNetworkApi {

    /** @var string $network */
    protected $network = 'clickky';

    /**
     * @Override
     * Gets url with auth appended, if any
     *
     * @param string $url
     * @return string
     */
    protected function getUrlAuth($url = '')
    {
        // Put as first or other qs param
        $url .= strrpos($url, "?") ? '&' : '?';

        // Append token to url
        return $url . 'hash=' . $this->auth_token;
    }

    /**
     * @Override
     * @param array $data
     * @return mixed
     */
    public function listOffers($data = array())
    {
        // Check we got page
        if (isset($data['page'])) {

            // Check we got amount, otherwise default to ...
            if (!isset($data['items_per_page'])) $data['items_per_page'] = NetworksManager::ITEMS_PER_PAGE;

            // Get offers from inherited one
            return parent::listOffers($data);
        }

        // Initial page
        $currPage = 1;

        // Total offers
        $totalOffers = array();

        // MAx items per page for this network
        $data['items_per_page'] = 1000;

        // Loop at least once
        do {

            // Set new path with paging
            $data['page'] = $currPage;

            // Get offers from inherited one
            $offers = parent::listOffers($data);

            // If something went wrong, just break here
            if (empty($offers) || empty($offers->offers)) break;

            // Add offers to collection
            $totalOffers = array_merge($totalOffers, $offers->offers);

        // Increment it while limit not reached
        } while ($currPage++ < $offers->pagecount);

        // Return final collection
        return (object) array('offers' => $totalOffers);
    }

    /**
     * @Override
     *
     * @param string $id
     * @param array $data
     * @return mixed
     */
    public function getOffer($id = '', $data = array())
    {
        // Get all offers first
        $offers = $this->payloadToOffers($this->listOffers($data));

        // Now filter this one
        $filtered = array_values(array_filter($offers['offers'], function(Offer $entry) use ($id) {
            return $entry->getExternalId() == $id;
        }));

        // Return found or null
        return count($filtered) ? $filtered[0] : null;
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return array
     */
    public function payloadToOffers($data = array('offers' => array()))
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Check input
        if (empty($data) || empty($data->offers)) return array();

        // TODO: There's an expiration date in offers, mostly null, maybe we should ask Clickky staff is this is relevant

        // Call parent with fixed data
        $offers = parent::payloadToOffers($data->offers);

        // Return with pages if any
        return array(
            'offers' => $offers,
            'pagination' => array(
                'pages_count' => isset($data->pagecount) ? $data->pagecount : 1,
                'total_count' => isset($data->available) ? $data->available : count($offers),
                'count' => isset($data->count) ? $data->count : count($offers),
                'page' => isset($data->pageindex) ? intval($data->pageindex, 10) : 1
            )
        );
    }

    /**
     * @Override
     * Parses response payload to offer
     *
     * @param array|stdClass $data
     * @return Offer
     */
    public function payloadToOffer($data = array())
    {
        // Normalize to object if not
        if (is_array($data)) $data = (object) $data;

        // Get underlying network
        $network = $this->getAffiliateNetwork();

        // Form tracking url
        $url = $data->link;
        $url .=  '&subid={clickid}';
        $url .= '&subid2={trafficsource.id}';

        // Get proper data from input
        $parsedData = array(
            'external_id' => $data->offer_id,
            'payout' => $data->payout,
            'countries' => isset($data->targeting->allowed->countries) ? $data->targeting->allowed->countries : array(),
            'url' => $url,
            'source_name' => isset($data->name) ? $data->name : '',
            'preview_url' => '',
            'preview_image' => isset($data->icon) ? $data->icon : '',
            // TODO: Improve this? Right now it's straightforward
            'platforms' => (isset($data->targeting) && isset($data->targeting->allowed) &&
                            isset($data->targeting->allowed->os)) ? $data->targeting->allowed->os : array(),
            'cost_model' => isset($data->offer_model) ? strtoupper($data->offer_model) : null,
            'caps' => isset($data->caps_daily) ? $data->caps_daily : null,
            // TODO: So an array of them, getting first one for now
            'creatives_url' => (isset($data->creative) && count($data->creative)) ? $data->creative[0]->url : null,
            'incent' => isset($data->traffic_type) && $data->traffic_type == 'incentive',
            // TODO:
            'operator_based' => $network->isOperatorBased(),
            'auto' => $network->isAuto(),
            'auto_payout' => $network->isAutoPayout()
        );

        // Call parent with fixed data
        return parent::payloadToOffer($parsedData);
    }
}