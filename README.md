Voluum Affiliate Integration
----------------------------

Advertising platform for integration with Voluum SaaS and third party affiliate networks
Features:
- Integration with Voluum network API for advertising tracking facilities
- Campaigns/Offers/Traffic Sources management thru web UI
- Integration of 3rd party affiliates APIs and management thru web UI
- Currently integrated 3rd party affiliates APIs are ArtOfClick, Clickky and Youappi
- Documented publishers API and Web UI app in pdf
