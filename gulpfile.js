var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var va_path = '../../../modules/voluum-affiliate/',
    va_assets_path = va_path + 'resources/assets/';

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix
        .less(va_assets_path + 'less/va-styles.less', 'public/css/va-styles.css')
        .version('css/va-styles.css');
});
